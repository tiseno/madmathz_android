package com.facebook.madmathz;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.madmathz.GameView.MyImageView;
import com.facebook.madmathz.connector.SessionStore;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BlurMaskFilter;
import android.graphics.BlurMaskFilter.Blur;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.app.Activity;
import android.view.Menu;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.AbsoluteLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class Splash extends Activity{
	LinearLayout container;
	//----------------------------------------------------------------------------
	private Animation myAnimation_Alpha;    //used to the alpha animation of logo_madmathz
	private Animation myAnimation_Rotate;   //used to the rotate animation of logo_oldmann1
	//----------------------------------------------------------------------------
	ImageView animView;//logo_madmathz
	ImageView animView1;//logo_oldmann1
	//-------------------------------------------------------------------------------
	Bitmap imgA;
	Bitmap imgM;
	Bitmap imgD;
	Bitmap imgT;
	Bitmap imgH;
	Bitmap imgZ;
	Bitmap imgLogo;
	Bitmap imgLogoOldMan;
	Bitmap imgDraw;
	int[] pointX = new int[22];
	int[] pointY = new int[22];
	Bitmap[] imgDrawing = new Bitmap[13];
	Bitmap imgShining ;
	int countTime = 0;
	int containerW = 0;
	int containerY = 0;
	Canvas canvas;
	Paint alphaPaint;
	public Handler mHandler=new Handler()  
    {  
        public void handleMessage(Message msg)  
        {  
            switch(msg.what)  
            {  
	            case 1:  
	            	//countTime--;
	            	container.invalidate();
	                break;  
	            case 2:
	            	  
	            	break;
	            default:  
	                break;            
            } 
            
           // my_layout.invalidate();  
           // super.handleMessage(msg);  
        }  
    };  
    
    ImageView img_title;
    ImageView img_logo_1;
    ImageView img_logo_2;
    ImageView img_logo_3;
    ImageView img_logo_4;
    ImageView img_logo_5;
    ImageView img_logo_6;
    ImageView img_logo_7;
    ImageView img_logo_8;
    
    ImageView img_shining;
    ImageView img_animview;
    ImageView animView2;
    RelativeLayout splash_pane;
    
    public void drawLogo()
    {
    	splash_pane = (RelativeLayout)findViewById(R.id.splash_pane);
    	img_title = (ImageView)findViewById(R.id.img_title);
    	img_logo_1 = (ImageView)findViewById(R.id.img_logo_1);
    	img_logo_2 = (ImageView)findViewById(R.id.img_logo_2);
    	img_logo_3 = (ImageView)findViewById(R.id.img_logo_3);
    	img_logo_4 = (ImageView)findViewById(R.id.img_logo_4);
    	img_logo_5 = (ImageView)findViewById(R.id.img_logo_5);
    	img_logo_6 = (ImageView)findViewById(R.id.img_logo_6);
    	img_logo_7 = (ImageView)findViewById(R.id.img_logo_7);
    	img_logo_8 = (ImageView)findViewById(R.id.img_logo_8);

    	img_title.setVisibility(View.INVISIBLE);
    	img_logo_1.setVisibility(View.INVISIBLE);
    	img_logo_2.setVisibility(View.INVISIBLE);
    	img_logo_3.setVisibility(View.INVISIBLE);
    	img_logo_4.setVisibility(View.INVISIBLE);
    	img_logo_5.setVisibility(View.INVISIBLE);
    	img_logo_6.setVisibility(View.INVISIBLE);
    	img_logo_7.setVisibility(View.INVISIBLE);
    	img_logo_8.setVisibility(View.INVISIBLE);
    	img_shining = (ImageView)findViewById(R.id.img_shining);
    	img_shining.setAlpha(100);
    	img_shining.setVisibility(View.INVISIBLE);
    	animView2 = new ImageView(this);
    	animView2.setImageResource(R.drawable.logo_oldmann1);
    	animView2.setVisibility(View.INVISIBLE);
    	splash_pane.addView(animView2);

    	Animation anim1 = new AlphaAnimation(0.0f, 1.0f);
    	anim1.setDuration(200);
    	img_logo_1.startAnimation(anim1);
    	
    	final Animation anim2 = new AlphaAnimation(0.0f, 1.0f);
    	anim2.setDuration(200);
    	
    	final Animation anim3 = new AlphaAnimation(0.0f, 1.0f);
    	anim3.setDuration(200);
    	
    	final Animation anim4 = new AlphaAnimation(0.0f, 1.0f);
    	anim4.setDuration(200);
    	
    	final Animation anim5 = new AlphaAnimation(0.0f, 1.0f);
    	anim5.setDuration(200);
    	
    	final Animation anim6 = new AlphaAnimation(0.0f, 1.0f);
    	anim6.setDuration(200);
    	
    	final Animation anim7 = new AlphaAnimation(0.0f, 1.0f);
    	anim7.setDuration(200);
    	
    	final Animation anim8 = new AlphaAnimation(0.0f, 1.0f);
    	anim8.setDuration(200);
    	
    	
    	final Animation title_anim = new AlphaAnimation(0.0f, 1.0f);
    	title_anim.setDuration(700);
    	
    	//Animation title_shining = new TranslateAnimation();
    	final AnimationSet animset_shine = new AnimationSet(true);
    	animset_shine.setFillAfter(true);
    	animset_shine.setFillEnabled(true);
    	final Animation shine_alpha = new AlphaAnimation(0.5f, 0.5f);
    	shine_alpha.setDuration(700);
    	
    	final Animation shining_anim = new TranslateAnimation(
    			0, containerW,img_shining.getTop(), img_shining.getTop());
    	shining_anim.setDuration(700);
    	animset_shine.addAnimation(shine_alpha);
    	animset_shine.addAnimation(shining_anim);
    	
    	//oldman
		RotateAnimation rotate = new RotateAnimation(0, -180, RotateAnimation.RELATIVE_TO_SELF, 0.0f, RotateAnimation.RELATIVE_TO_SELF, 0.0f);
		rotate.setDuration(400);
	
		final Animation credit_alpha = new AlphaAnimation(1.0f, 1.0f);
    	credit_alpha.setDuration(400);
    	
    	Animation trans = new TranslateAnimation(containerW, containerW - animView2.getWidth() - 20,containerY,  containerY - animView2.getHeight() - 30);
    	trans.setDuration(400);

    	final AnimationSet animset = new AnimationSet(true);
    	animset.setFillAfter(true);
    	animset.setFillEnabled(true);
    	animset.addAnimation(credit_alpha);
    	animset.addAnimation(rotate);
    	animset.addAnimation(trans);
    	

    	anim1.setAnimationListener(new AnimationListener(){

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				img_logo_1.setVisibility(View.VISIBLE);
				img_logo_2.startAnimation(anim2);
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				
			}});
    	
    	anim2.setAnimationListener(new AnimationListener(){

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				img_logo_3.startAnimation(anim3);
				img_logo_2.setVisibility(View.VISIBLE);

			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				
			}});
    	
    	anim3.setAnimationListener(new AnimationListener(){

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				img_logo_4.startAnimation(anim4);
				img_logo_3.setVisibility(View.VISIBLE);

			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				
			}});
    	
    	anim4.setAnimationListener(new AnimationListener(){

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				img_logo_5.startAnimation(anim5);
				img_logo_4.setVisibility(View.VISIBLE);

			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				
			}});
    	anim5.setAnimationListener(new AnimationListener(){

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				img_logo_6.startAnimation(anim6);
				img_logo_5.setVisibility(View.VISIBLE);

			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				
			}});
    	
    	anim6.setAnimationListener(new AnimationListener(){

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				img_logo_7.startAnimation(anim7);
				img_logo_6.setVisibility(View.VISIBLE);

			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				
			}});
    	
    	anim7.setAnimationListener(new AnimationListener(){

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				img_logo_8.startAnimation(anim8);
				img_logo_7.setVisibility(View.VISIBLE);

			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				
			}});
    	
    	anim8.setAnimationListener(new AnimationListener(){

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
		    	img_title.startAnimation(title_anim);
				img_logo_8.setVisibility(View.VISIBLE);


			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				
			}});
    	title_anim.setAnimationListener(new AnimationListener(){

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				img_title.setVisibility(View.VISIBLE);
				img_shining.setVisibility(View.VISIBLE);
				animView2.setVisibility(View.VISIBLE);

		    	img_shining.startAnimation(animset_shine);
		    	animView2.startAnimation(animset);
		    	
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				
			}});
    	shining_anim.setAnimationListener(new AnimationListener(){

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				img_shining.setVisibility(View.INVISIBLE);
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				
			}});
    	
    }
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_splash);
//		CookieSyncManager.createInstance(getApplicationContext()); 

		
		//add view to containerLayout
		this.container = (LinearLayout) findViewById(R.id.madmathzLayout);
		Display localDisplay = getWindowManager().getDefaultDisplay();
		containerW = localDisplay.getWidth();
		containerY = localDisplay.getHeight();
		
		drawLogo();
		
		mHandler.postDelayed(mInitGame, 5000);
		/*
		animView = (ImageView) findViewById(R.id.animView1);
		animView1 = (ImageView) findViewById(R.id.animView2);
		
		alphaPaint = new Paint();
		alphaPaint.setColor(Color.rgb(232, 232, 233));	
		alphaPaint.setMaskFilter(new BlurMaskFilter(25, Blur.INNER));
		//alphaPaint.setAlpha(220);
		this.canvas = new Canvas();
		this.containerview = new MyContainerView(this);
		//containerview.layout(l, t, r, b);
		this.container.addView(this.containerview); 
		*/
		
		//read setting
		
		Preferences.loadPreferences(getApplicationContext());
        playBGM();
	}
	
	initGameThread mInitGame = new initGameThread();
	
	private class initGameThread implements Runnable{

		@Override
		public void run() {
			// TODO Auto-generated method stub
			initGame();
		}
		
	}
	
	public void DrawAlpha()
	{
		
	}
	
	
	
	
	
	
	
	public void playBGM(){
		
		if(Preferences.bBGM)
			startService(new Intent(this, MediaPlayerService.class));
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_splash, menu);
		return true;
	}

	public void initGame()
	{
		if(Preferences.isIncomingCall) return;
		//init preferences
		//switch activity
		
		CookieSyncManager cookieSyncMngr = 
				CookieSyncManager.createInstance(this); 
		CookieManager cookieManager = CookieManager.getInstance(); 
    	CookieSyncManager.getInstance().sync(); 

		//cookieManager.removeAllCookie();

        Utility.mFacebook = new Facebook(getResources().getString(R.string.app_id));
        if(Preferences.access_token != null)
        {
        	Utility.mFacebook.setAccessToken(Preferences.access_token);
        }
        
        if(Preferences.access_expires != 0)
        {
        	Utility.mFacebook.setAccessExpires(Preferences.access_expires);
        	
        }
        
        // Instantiate the asynrunner object for asynchronous api calls.
        Utility.mAsyncRunner = new AsyncFacebookRunner(Utility.mFacebook);
        //if(SessionStore.restore(Utility.mFacebook, getApplicationContext()))
        if(Utility.mFacebook.isSessionValid() && Utility.isNetworkReacheable(this))
		{
        	
			Intent intent = new Intent(this, MadMathz.class);		 
   		    startActivity(intent);
   		    isContinue = true;
   		    this.finish();
		}
		else
		{
			Intent intent = new Intent(this, LogoView.class);		 
   		    startActivity(intent);
   		    isContinue = true;
   		    this.finish();
			
			
		}
 	}
	
	protected void onResume()
	{
		super.onResume();
		
		if(Preferences.isIncomingCall)
		{
			Preferences.isIncomingCall = false;
			if(Preferences.bBGM) startService(new Intent(this, MediaPlayerService.class));
			
			mHandler.post(new Runnable(){ //call process

				@Override
				public void run() {
					// TODO Auto-generated method stub
					
			    	LayoutInflater mInflater = (LayoutInflater) Splash.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			    	View inflatedView = mInflater.inflate(R.layout.phone_call, null);
			    	
			    	final RelativeLayout loadingView = (RelativeLayout)findViewById(R.id.loadingView);
			    	loadingView.setVisibility(View.VISIBLE);
			    	loadingView.setBackgroundColor(0xB2000000);
			    	loadingView.removeAllViews();
			    	
			    	loadingView.addView(inflatedView);
			    	
			    	ImageButton btn_resume = (ImageButton)inflatedView.findViewById(R.id.btn_resume);
			    	btn_resume.setOnTouchListener(new ButtonHighlight(btn_resume));
			    	btn_resume.setOnClickListener(new OnClickListener(){

						@Override
						public void onClick(View arg0) {
							// TODO Auto-generated method stub
							loadingView.setVisibility(View.INVISIBLE);
							loadingView.removeAllViews();
							
							initGame();
						}});
			    	
			    	ImageButton btn_quit = (ImageButton)inflatedView.findViewById(R.id.btn_quit);
			    	btn_quit.setOnTouchListener(new ButtonHighlight(btn_quit));
			    	
			    	btn_quit.setOnClickListener(new OnClickListener(){

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							stopService(new Intent(Splash.this, MediaPlayerService.class));
							Splash.this.finish();
							
						}});
				}});
		}
	}
	
	public boolean isContinue = false;
	public boolean stopGame = false;
	
	protected void onStop()
	{
		super.onStop();
		if(Preferences.isIncomingCall)
		{
			mHandler.removeCallbacks(mInitGame);
			stopService(new Intent(Splash.this, MediaPlayerService.class));
		}
		else if(!isContinue)
		{
			mHandler.removeCallbacks(mInitGame);
			stopService(new Intent(Splash.this, MediaPlayerService.class));
			this.finish();
		}
		
		
	}
	

}
