package com.facebook.madmathz;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.madmathz.FriendsList.PostDialogListener;
import com.facebook.madmathz.GameView.MyImageView;
import com.facebook.madmathz.connector.BaseDialogListener;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.Window;
import android.view.View.OnTouchListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Typeface;
public class StartInGame extends Activity implements View.OnClickListener{
	//--------mediaPlayer instance-----------
	MediaPlayer sfLose;
	MediaPlayer sfWin;
	//---------------------------------------
	ImageView imgStartView;
	ImageButton imgHomeView;
	float eX;
	public String mSelView;
	private SharedPreferences sharedPrefrences;
	private Editor editor;
	int yourPoint;
	TextView mTxtView;
	TextView mPoint;
	TextView UserHelloTxt;
	RelativeLayout startingView;
	RelativeLayout messageBoxView;
//	ImageButton mBtnSubmit;
	ImageView thumbnail;
	EditText editTextToFriend;
	LinearLayout GameResultView;
	TextView txtWhatTo;
	
	//int GameType = 0;
	int GameState = 0;

	
	public static final int TYPE_SINGLEGAME = 0;
	public static final int TYPE_CHALLENGEGAME = 1;
	public static final int TYPE_ACCEPTGAME = 2;
	public static final int TYPE_RECHALLENGE = 3;
	
	public static final int STATE_START = 0;
	public static final int STATE_END = 1;
	
	public static final int RESULT_WIN = 1;
	public static final int RESULT_LOSE = 0;
	public static final int RESULT_DRAW = 2;
	public static final int RESULT_ISFRIENDTURN = 3;
	
	public static final int LOADING = 1000;
	public static final int LOADING_FINISH = 2000;
	public static final int SHOW_COMPRESULT = 3000;
	public static final int LOADING_GAMEEND = 4000;
	
	public boolean isFPlayer = true;
	
	
	
	public void GameResultSound(int result)
	{
		if(result == RESULT_WIN)
		{
			if(!Preferences.bSound) return;
			this.sfWin.start();
		}
		else if(result == RESULT_LOSE)
		{
			if(!Preferences.bSound) return;
			this.sfLose.start();
		}
		
	}
	
	public void DrawGameStart()
	{
		RelativeLayout StartingView = (RelativeLayout)findViewById(R.id.startingView);
		ImageView img = (ImageView)findViewById(R.id.imgStart);
		img.setImageResource(R.drawable.screen_start);
		
		this.drawLevelSelect();		
		
		StartingView.setOnClickListener(this);
		
		if(Preferences.cur_ChallengeList.GameType == TYPE_ACCEPTGAME)
		{
			mHandler.postDelayed(new Runnable(){

				@Override
				public void run() {
					// TODO Auto-generated method stub
					drawChallengerMessage();
					
				}}, 1000);
		}
		
	}
	
	public void drawMessage()
	{

		if(Preferences.cur_ChallengeList.GameType == TYPE_RECHALLENGE)
		{
			//retrieve game result of so far
			mHandler.post(new Runnable(){

				@Override
				public void run() {
					// TODO Auto-generated method stub
					HashMap<String, String> params = new HashMap<String, String>();
					params.put("FID", Preferences.cur_ChallengeList.fplayer_id);
					params.put("SID", Preferences.cur_ChallengeList.splayer_id);
					String response = HttpManager.postHttpResponse(URI.create(Preferences.getTotalMatchResult), params);
					
					if(response != "")
					{
						try
						{
							JSONArray json_arr = new JSONObject(response).getJSONArray("posts");
							
							if(json_arr.length() > 0)
							{
								gameCount = json_arr.length();
								for(int i = 0; i < json_arr.length(); i++)
								{
									if(json_arr.getJSONObject(i).getJSONObject("totalMacthResult").getString("winner").equals(Preferences.FBID))
									{
										winCount++;
									}
								}
								
								drawMatchResultBoard();
							}
						}
						catch(Exception e){}
					}
					
				}});
		}
	}
	
	public void drawChallengerMessage()
	{
		if(Preferences.cur_ChallengeList.fplayer_msg.equals(""))
		{
			return;
		}
		
		//RelativeLayout MessageMultiResult = (RelativeLayout)findViewById(R.id.MessageMultiResult);
		
		LinearLayout challengeMessageBoard = (LinearLayout)findViewById(R.id.challengeMessageBoard);
		challengeMessageBoard.setVisibility(View.VISIBLE);
		
		TextView challengerName = (TextView)findViewById(R.id.challengerName);
		TextView challengerMsg = (TextView)findViewById(R.id.challengerMsg);
		challengerName.setTypeface(Preferences.tf_bold);
		challengerName.setTextColor(Preferences.tx_color);
		challengerName.setText(Preferences.cur_ChallengeList.fplayer_username + " said");
		
		challengerMsg.setTypeface(Preferences.tf_normal);
		challengerMsg.setTextColor(Preferences.tx_color);
		challengerMsg.setText(Preferences.cur_ChallengeList.fplayer_msg);
		
		Animation animation = new TranslateAnimation(0, 0, 100, 0);
		animation.setDuration(1000);
		challengeMessageBoard.startAnimation(animation);
	}
	
	
	public int winCount = 0;
	public int gameCount = 0;
	
	public void drawMatchResultBoard()
	{
		RelativeLayout multiResultBoard = (RelativeLayout)findViewById(R.id.multiResultBoard);
		RelativeLayout MessageMultiResult = (RelativeLayout)findViewById(R.id.MessageMultiResult);
		multiResultBoard.setVisibility(View.VISIBLE);
		TextView multiResultTxt = (TextView)findViewById(R.id.multiResultTxt);
		multiResultTxt.setTypeface(Preferences.tf_normal);
		multiResultTxt.setTextColor(Preferences.tx_color);
		multiResultTxt.setText(String.format("You won %d out of %d games between %s.", winCount, gameCount, Preferences.cur_ChallengeList.splayer_username));
		
		//animation
		//Animation animation = new TranslateAnimation(0, 0, MessageMultiResult.getBottom(), MessageMultiResult.getBottom() - multiResultBoard.getHeight());
		Animation animation = new TranslateAnimation(0, 0, 100, 0);
		animation.setDuration(1000);
		multiResultBoard.startAnimation(animation);
	}
	
	public int splayerscore = 0;
	
	public void judgeGameResult()
	{
		
		 addPoint = 0;
		 
		 boolean isDualWin100 = false;
		 boolean isDualLose100 = false;
		 boolean isSingleWin100 = false;
		 
		 if(Preferences.cur_ChallengeList.GameType != TYPE_SINGLEGAME)
		 {
			 try {
				 splayerscore = Integer.parseInt(Preferences.AllUserInfo.get(Preferences.cur_ChallengeList.fplayer_id));
			 } catch (Exception e) {}
		 }
		 else
		 {
			 splayerscore = 0;
		 }
		 
		 
		 
		 
		if(Preferences.cur_ChallengeList.GameType == TYPE_CHALLENGEGAME ||
				Preferences.cur_ChallengeList.GameType == TYPE_RECHALLENGE)
		{
			Preferences.cur_ChallengeList.GameResult = RESULT_ISFRIENDTURN;
		}
		else if(Preferences.cur_ChallengeList.GameType == TYPE_ACCEPTGAME)
		{
			//
			if(Preferences.cur_ChallengeList.fplayer_comp_poss > Preferences.cur_ChallengeList.splayer_comp_poss)
			{
				Preferences.cur_ChallengeList.WinnerID = Preferences.cur_ChallengeList.fplayer_id;
				Preferences.cur_ChallengeList.GameResult = RESULT_LOSE;
				splayerscore += 2;
				
				isDualLose100 = (splayerscore %100 == 0) ? true: false;
			}
			else if(Preferences.cur_ChallengeList.fplayer_comp_poss < Preferences.cur_ChallengeList.splayer_comp_poss)
			{
				addPoint = 2;
				Preferences.Score += addPoint;
				Preferences.cur_ChallengeList.WinnerID = Preferences.cur_ChallengeList.splayer_id;
				Preferences.cur_ChallengeList.GameResult = RESULT_WIN;
				isDualWin100 = (Preferences.Score %100 == 0) ? true: false;
			}
			else
			{
				if(Preferences.cur_ChallengeList.fplayer_comp_time > Preferences.cur_ChallengeList.splayer_comp_time)
				{
					addPoint = 2;
					Preferences.cur_ChallengeList.WinnerID = Preferences.cur_ChallengeList.splayer_id;
					Preferences.cur_ChallengeList.GameResult = RESULT_WIN;
					Preferences.Score += addPoint;
					isDualWin100 = (Preferences.Score %100 == 0) ? true: false;

				}
				else if(Preferences.cur_ChallengeList.fplayer_comp_time < Preferences.cur_ChallengeList.splayer_comp_time)
				{
					Preferences.cur_ChallengeList.WinnerID = Preferences.cur_ChallengeList.fplayer_id;
					Preferences.cur_ChallengeList.GameResult = RESULT_LOSE;
					splayerscore += 2;
					isDualLose100 = (splayerscore %100 == 0) ? true: false;

				}
				else
				{
					if(Preferences.cur_ChallengeList.fplayer_wrong_tried > Preferences.cur_ChallengeList.splayer_wrong_tried)
					{
						addPoint = 2;
						Preferences.cur_ChallengeList.GameResult = RESULT_WIN;
						Preferences.cur_ChallengeList.WinnerID = Preferences.cur_ChallengeList.splayer_id;
						Preferences.Score += addPoint;
						isDualWin100 = (Preferences.Score %100 == 0) ? true: false;

					}
					else if(Preferences.cur_ChallengeList.fplayer_wrong_tried < Preferences.cur_ChallengeList.splayer_wrong_tried)
					{
						Preferences.cur_ChallengeList.WinnerID = Preferences.cur_ChallengeList.fplayer_id;
						Preferences.cur_ChallengeList.GameResult = RESULT_LOSE;
						splayerscore += 2;
						isDualLose100 = (splayerscore %100 == 0) ? true: false;
					}
					else
					{
						if(Preferences.cur_ChallengeList.fplayer_repeat > Preferences.cur_ChallengeList.splayer_repeat)
						{
							addPoint = 2;
							Preferences.cur_ChallengeList.WinnerID = Preferences.cur_ChallengeList.splayer_id;
							Preferences.cur_ChallengeList.GameResult = RESULT_WIN;
							Preferences.Score += addPoint;
							isDualWin100 = (Preferences.Score %100 == 0) ? true: false;

						}
						else if(Preferences.cur_ChallengeList.fplayer_repeat < Preferences.cur_ChallengeList.splayer_repeat)
						{
							Preferences.cur_ChallengeList.WinnerID = Preferences.cur_ChallengeList.fplayer_id;
							Preferences.cur_ChallengeList.GameResult = RESULT_LOSE;
							splayerscore += 2;
							isDualLose100 = (splayerscore %100 == 0) ? true: false;

						}
						else
						{
							Preferences.cur_ChallengeList.GameResult = RESULT_DRAW;
							Preferences.cur_ChallengeList.WinnerID = "";
						}
					}

				}

				
			}
			
			if(Preferences.cur_ChallengeList.GameResult == RESULT_WIN)
			{
				sendUpdateScore(Preferences.FBID, Preferences.Score);
			}
			else if(Preferences.cur_ChallengeList.GameResult == RESULT_LOSE)
			{
				sendUpdateScore(Preferences.cur_ChallengeList.fplayer_id, splayerscore);
				
			}
		}
		else if(Preferences.cur_ChallengeList.GameType == TYPE_SINGLEGAME)
		{
			if(Preferences.cur_ChallengeList.fplayer_comp_poss >= Preferences.cur_ChallengeList.possibility)
			{
				addPoint = 1;
				Preferences.Score += addPoint;
				isSingleWin100 = (Preferences.Score %100 == 0) ? true: false;

				Preferences.cur_ChallengeList.GameResult = RESULT_WIN;
				Preferences.cur_ChallengeList.result = 1;
				sendUpdateScore(Preferences.FBID, Preferences.Score);
			}
			else if(Preferences.cur_ChallengeList.fplayer_comp_poss < Preferences.cur_ChallengeList.splayer_comp_poss)
			{
				Preferences.cur_ChallengeList.GameResult = RESULT_LOSE;
				Preferences.cur_ChallengeList.result = 0;
			}
		}
		
		
		if(isSingleWin100 || isDualWin100 && Preferences.Score > 0)
		{
			send100Post(Preferences.UserName);
		}
		
		if(isDualLose100 && Preferences.cur_ChallengeList.fplayer_score > 0)
		{
			send100Post(Preferences.cur_ChallengeList.fplayer_username);
		}
		
		
		
	}
	
	public void sendUpdateScore(final String fb_id, final int score)
	{
		
		mHandler.post(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				HashMap<String, String> params = new HashMap<String, String>();
				params.put("Score", String.valueOf(score));
				params.put("FBID", fb_id);
				//params.add(new BasicNameValuePair("Score", String.valueOf(Preferences.Score)));
				//params.add(new BasicNameValuePair("FBID", Preferences.FBID));
				String result = HttpManager.postHttpResponse(URI.create(Preferences.updateUserScore), params);
				
			}});
		
	
	}
	
	public void send100Post(final String name)
	{

   		new AlertDialog.Builder(this).setTitle("Post On Wall")
           .setMessage(String.format("%s post on wall", name))
           .setPositiveButton("Post", new DialogInterface.OnClickListener() {
               @Override
               public void onClick(DialogInterface dialog, int which) {
               	Bundle bundle = new Bundle();
           		bundle.putString("link", "www.facebook.com/Madmathz");
           		bundle.putString("picture", "http://zrlim.com/mm/logo.png");
           		bundle.putString("name", "MadMathz");
           		bundle.putString("caption", "Game Descriptions");
           		bundle.putString("description", String.format("%s hit the hundred pointer !!", name));
                   Utility.mFacebook.dialog(StartInGame.this, "feed", bundle,
                           new PostDialogListener());
               }

           }).setNegativeButton("Cancel", null).show();
           
	}
	
	
	
	public void DrawGameEnd()
	{
		mPoint = (TextView)findViewById(R.id.UserPointsText);
		String txt = "";

		ImageView img = (ImageView)findViewById(R.id.imgStart);
		RelativeLayout StartingView = (RelativeLayout)findViewById(R.id.startingView);
		StartingView.setOnClickListener(this);
		switch(Preferences.cur_ChallengeList.GameResult)
		{
			case RESULT_ISFRIENDTURN:
			{
				img.setImageResource(R.drawable.screen_waiting);
				 mPoint.setText(String.format("You get %d combo in %d seconds", Preferences.cur_ChallengeList.fplayer_comp_poss, Preferences.cur_ChallengeList.fplayer_comp_time));

			}
				break;
			case RESULT_WIN:
			{
				img.setImageResource(R.drawable.screen_win);
				
				txt = String.format("You have granted %d points", addPoint);
				mPoint.setText(txt);
				
				if(Preferences.cur_ChallengeList.GameType == TYPE_SINGLEGAME)
				{
				}
				

				mHandler.postDelayed(new Runnable(){

					@Override
					public void run() {
						// TODO Auto-generated method stub
						transitionstar();
						GameResultSound(RESULT_WIN);
					}}, 2500);
			}
				break;
			case RESULT_LOSE:
			{
				img.setImageResource(R.drawable.screen_lose);
				
				txt = "You lose this game!";
				mPoint.setText(txt);

				
				mHandler.postDelayed(new Runnable(){

					@Override
					public void run() {
						// TODO Auto-generated method stub
						lighteningblinking();
						GameResultSound(RESULT_LOSE);
					}}, 2500);
			}
				break;
			case RESULT_DRAW:
			{
				img.setImageResource(R.drawable.screen_draw);
				
				txt = "It's a draw!";
				mPoint.setText(txt);


			}
				break;
		}
		
		if(Preferences.cur_ChallengeList.GameType == TYPE_ACCEPTGAME)
		{
			initGameResultView();
			mHandler.postDelayed(new Runnable(){

				@Override
				public void run() {
					// TODO Auto-generated method stub
					
					transitionresultbg();
				}
			}, 4000);
		}
		
		if(Preferences.cur_ChallengeList.GameType != TYPE_SINGLEGAME)
		{
			mHandler.postDelayed(new Runnable(){

				@Override
				public void run() {
					// TODO Auto-generated method stub
					drawMessageBox();
				}}, 1500);
			
		}
		
	}
	
	boolean bDrawLevel = false;
	
	public void drawLevelSelect()
	{
		if(Preferences.cur_ChallengeList.GameType == TYPE_ACCEPTGAME) return;
		bDrawLevel = true;
		RelativeLayout level_select = (RelativeLayout) findViewById(R.id.level_select);
		level_select.setBackgroundColor(0x99000000);
		level_select.setVisibility(View.VISIBLE);
	    
	    ImageButton level_easy_btn = (ImageButton)findViewById(R.id.level_easy_btn);
	    ImageButton level_middle_btn = (ImageButton)findViewById(R.id.level_middle_btn);
	    ImageButton level_hard_btn = (ImageButton)findViewById(R.id.level_hard_btn);
	    
	    level_easy_btn.setOnClickListener(this);
	    level_easy_btn.setOnTouchListener(new ButtonHighlight(level_easy_btn));
	    
	    level_middle_btn.setOnClickListener(this);
	    level_middle_btn.setOnTouchListener(new ButtonHighlight(level_middle_btn));
	    
	    level_hard_btn.setOnClickListener(this);
	    level_hard_btn.setOnTouchListener(new ButtonHighlight(level_hard_btn));
	    

	}
	
	
	//if score / 100 == 0
	public void postStatus()
	{
		
		if(Preferences.Score / 100 == 0 && Preferences.cur_ChallengeList.GameResult == RESULT_WIN)
		{

       		new AlertDialog.Builder(this).setTitle("Post On Wall")
               .setMessage(String.format("%s post on wall", Preferences.UserName))
               .setPositiveButton("Post", new DialogInterface.OnClickListener() {
                   @Override
                   public void onClick(DialogInterface dialog, int which) {
                   	Bundle bundle = new Bundle();
               		bundle.putString("link", "www.facebook.com/Madmathz");
               		bundle.putString("picture", "http://zrlim.com/mm/logo.png");
               		bundle.putString("name", "MadMathz");
               		bundle.putString("caption", "You will crazy in one minute");
               		bundle.putString("description", "You hit the hundred pointer !!");
                       Utility.mFacebook.dialog(StartInGame.this, "feed", bundle,
                               new PostDialogListener());
                   }

               }).setNegativeButton("Cancel", null).show();
		}
		
	}
	
	   public class PostDialogListener extends BaseDialogListener {
	        @Override
	        public void onComplete(Bundle values) {
	            final String postId = values.getString("post_id");
	            if (postId != null) {
	                showToast("Message posted on the wall.");
	            } else {
	                showToast("No message posted on the wall.");
	            }
	        }
	    }

	    public void showToast(final String msg) {
	        mHandler.post(new Runnable() {
	            @Override
	            public void run() {
	                Toast toast = Toast.makeText(StartInGame.this, msg, Toast.LENGTH_LONG);
	                toast.show();
	            }
	        });
	    }
	
	
	Handler mHandler = new Handler(){
		public void HandleMessage(Message msg)
		{
			switch(msg.what)
			{
			case LOADING:
				break;
			case LOADING_FINISH:
				break;
			case SHOW_COMPRESULT:
				break;
			
			}
		}
	};
	
	 int addPoint = 0;
	 
	 public void drawMessageBox()
	 {
		 messageBoxView.setVisibility(View.VISIBLE);
		 LinearLayout messagebox = (LinearLayout)findViewById(R.id.messagebox);

		 messageBoxView.setVisibility(View.VISIBLE);
		 
	    ImageButton mBtnSubmit = (ImageButton)findViewById(R.id.mBtnSubmit);
	    mBtnSubmit.setOnClickListener(this);
	    mBtnSubmit.setOnTouchListener(new ButtonHighlight(mBtnSubmit));
	    
		txtWhatTo = (TextView)findViewById(R.id.txtWhatTo);
		txtWhatTo.setTextColor(Preferences.tx_color);
		txtWhatTo.setTypeface(Preferences.tf_normal);
		
		TextView txtWhom = (TextView)findViewById(R.id.txtWhom);
		txtWhom.setTextColor(Preferences.tx_color);
		txtWhom.setTypeface(Preferences.tf_normal, Typeface.BOLD);
		
		if(Preferences.FBID.equals(Preferences.cur_ChallengeList.fplayer_id))
			txtWhom.setText(Preferences.cur_ChallengeList.splayer_username);
		else
			txtWhom.setText(Preferences.cur_ChallengeList.fplayer_username);


		 
		 int height = getWindowManager().getDefaultDisplay().getHeight();
		 
		 TranslateAnimation moveLefttoRight = new TranslateAnimation(0, 0, 100, 0);
	     moveLefttoRight.setDuration(1000);
         messagebox.startAnimation(moveLefttoRight);
         moveLefttoRight.setAnimationListener(new AnimationListener(){

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				
			}});
	 }

	
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.gamestart);
		
		
		mTxtView =(TextView)findViewById(R.id.UserHelloText);
	    
		Intent intent = getIntent();
		 
		 //GameType = intent.getIntExtra("GameType", TYPE_SINGLEGAME);
		 GameState = intent.getIntExtra("GameState", STATE_START);
		// GameResult = intent.getIntExtra("GameResult", RESULT_WIN);
		// GameTime = intent.getIntExtra("GameTime", 60);
		 
		 this.messageBoxView = (RelativeLayout)findViewById(R.id.MessageBoardView);
		 this.imgHomeView = (ImageButton)findViewById(R.id.imgHome);
		 this.imgHomeView.setOnClickListener(this);
		 this.imgHomeView.setOnTouchListener(new ButtonHighlight(this.imgHomeView));
		 
		 this.startingView = (RelativeLayout) findViewById(R.id.startingView);
		 startingView.setOnClickListener(this);
		 this.thumbnail = (ImageView)findViewById(R.id.thumbnail);
		 this.UserHelloTxt = (TextView)findViewById(R.id.UserHelloText);
		 UserHelloTxt.setTextColor(Preferences.tx_color);
		 UserHelloTxt.setTypeface(Preferences.tf_normal, Typeface.BOLD);
		 
		 this.sfLose = MediaPlayer.create(getApplicationContext(), R.raw.lose);
		 this.sfWin = MediaPlayer.create(getApplicationContext(), R.raw.win);
		 
		 //mBtnSubmit = (ImageButton)findViewById(R.id.mBtnSubmit);
		 //mBtnSubmit.setOnClickListener(this);
//	    LayoutInflater mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//	    View inflatedView = mInflater.inflate(R.layout.messageboard_layout, null);

		 
		 this.editTextToFriend = (EditText)findViewById(R.id.editTextToFriend);
		 editTextToFriend.setOnKeyListener(new OnKeyListener() {
			    public boolean onKey(View v, int keyCode, KeyEvent event) {
			        // If the event is a key-down event on the "enter" button
			        if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
			            (keyCode == KeyEvent.KEYCODE_ENTER)) {
			        	InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
	                    imm.hideSoftInputFromWindow(editTextToFriend.getWindowToken(),
	                            0);
	                  
			          return true;
			        }
			        return false;
			    }
			});
		 GameResultView = (LinearLayout)findViewById(R.id.GameResult);
		 //Init Header 
		 
		mPoint = (TextView)findViewById(R.id.UserPointsText);
		mPoint.setTextColor(Preferences.tx_color);
		mPoint.setTypeface(Preferences.tf_normal);
		String strPointer = "You have " + Preferences.Score + " points";
		mPoint.setText(strPointer);
		mPoint.setTypeface(Preferences.tf_normal); 
		UserHelloTxt.setText(Preferences.UserName);
		
		Display localDisplay = StartInGame.this.getWindowManager().getDefaultDisplay();
		loadingView = (RelativeLayout) findViewById(R.id.LoadingView);
	    
	    star = new ImageView(this);
	    star1 = new ImageView(this);
	    star2 = new ImageView(this);
	    star3 = new ImageView(this);
	    star4 = new ImageView(this);
	    star5 = new ImageView(this);
	    
	    star.setVisibility(View.INVISIBLE);
	    star1.setVisibility(View.INVISIBLE);
	    star2.setVisibility(View.INVISIBLE);
	    star3.setVisibility(View.INVISIBLE);
	    star4.setVisibility(View.INVISIBLE);
	    star5.setVisibility(View.INVISIBLE);
	    
		startingView.addView(star);
		startingView.addView(star1);
		startingView.addView(star2);
		startingView.addView(star3);
		startingView.addView(star4);
		startingView.addView(star5);
		
		//lightening = new ImageView(this);
		lightening = (ImageView)findViewById(R.id.lightening);
		lightening.setImageResource(R.drawable.lightning);
		//startingView.addView(lightening);
		lightening.setVisibility(View.INVISIBLE);
		final LinearLayout GameResult = (LinearLayout)findViewById(R.id.GameResult);
		GameResult.setVisibility(View.INVISIBLE);

		

		
		mHandler.post(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				thumbnail.setTag("http://graph.facebook.com/" + Preferences.FBID + "/picture");
				new GetPicAsyncTask().execute(thumbnail);
				
			}
			
		});
		//thumbnail.setImageBitmap(Preferences.mUserPic);
		 if(GameState == STATE_START)
		 {
			 DrawGameStart();
			 
		 }
		 if(GameState == STATE_END)
		 {
			judgeGameResult();
			DrawGameEnd();
		 }
	}
	
	public void initGameResultView()
	{
		 ImageView imgFriend1 = (ImageView)findViewById(R.id.imgFriend1);
		 imgFriend1.setTag(String.format(Preferences.FacebookPicUrl, Preferences.cur_ChallengeList.fplayer_id));
		 new GetPicAsyncTask().execute(imgFriend1);
		 
		 ImageView imgFriend2 = (ImageView)findViewById(R.id.imgFriend2);
		 imgFriend2.setTag(String.format(Preferences.FacebookPicUrl, Preferences.cur_ChallengeList.splayer_id));
		 new GetPicAsyncTask().execute(imgFriend2);
		 
		 TextView txtScore1 = (TextView)findViewById(R.id.txtScore1);
		 txtScore1.setTypeface(Preferences.tf_bold);
		 txtScore1.setTextSize(14);
		 txtScore1.setText("" + Preferences.cur_ChallengeList.fplayer_comp_poss);
		 
		 TextView txtScore2 = (TextView)findViewById(R.id.txtScore2);
		 txtScore2.setTypeface(Preferences.tf_bold);
		 txtScore2.setTextSize(14);
		 txtScore2.setText("" + Preferences.cur_ChallengeList.splayer_comp_poss);
		 
		 TextView txtRepeat1 = (TextView)findViewById(R.id.txtRepeat1);
		 txtRepeat1.setTypeface(Preferences.tf_bold);
		 txtRepeat1.setTextSize(14);
		 txtRepeat1.setText("" + Preferences.cur_ChallengeList.fplayer_repeat);
		 
		 TextView txtRepeat2 = (TextView)findViewById(R.id.txtRepeat2);
		 txtRepeat2.setTypeface(Preferences.tf_bold);
		 txtRepeat2.setTextSize(14);
		 txtRepeat2.setText("" + Preferences.cur_ChallengeList.splayer_repeat);
		 
		 TextView txtWrong1 = (TextView)findViewById(R.id.txtWrong1);
		 txtWrong1.setTypeface(Preferences.tf_bold);
		 txtWrong1.setTextSize(14);
		 txtWrong1.setText("" + Preferences.cur_ChallengeList.fplayer_wrong_tried);
		 
		 TextView txtWrong2 = (TextView)findViewById(R.id.txtWrong2);
		 txtWrong2.setTypeface(Preferences.tf_bold);
		 txtWrong2.setTextSize(14);
		 txtWrong2.setText("" + Preferences.cur_ChallengeList.splayer_wrong_tried);

		 TextView txtTime1 = (TextView)findViewById(R.id.txtTime1);
		 txtTime1.setTypeface(Preferences.tf_bold);
		 txtTime1.setTextSize(14);
		 txtTime1.setText("" + Preferences.cur_ChallengeList.fplayer_comp_time);
		 
		 TextView txtTime2 = (TextView)findViewById(R.id.txtTime2);
		 txtTime2.setTypeface(Preferences.tf_bold);
		 txtTime2.setTextSize(14);
		 txtTime2.setText("" + Preferences.cur_ChallengeList.splayer_comp_time);
		 

	}
	
	public void transitionresultbg()
	{
		LinearLayout GameResult = (LinearLayout)findViewById(R.id.GameResult);
		GameResult.setBackgroundColor(0x99000000);
		GameResult.setVisibility(View.VISIBLE);
		
		LinearLayout result_img = (LinearLayout)findViewById(R.id.result_img);
		result_img.setVisibility(View.VISIBLE);
		
		TableRow score_row = (TableRow)findViewById(R.id.score_row);
		TableRow repeat_row = (TableRow)findViewById(R.id.repeat_row);
		TableRow wrong_row = (TableRow)findViewById(R.id.wrong_row);
		TableRow time_row = (TableRow)findViewById(R.id.time_row);
		
		Animation bg_transition = new TranslateAnimation(0f, 0f, 100, 0);
		bg_transition.setDuration(300);
		GameResult.startAnimation(bg_transition);
		
		
		
		
		Animation animation = new AlphaAnimation(1.0f, 1.0f);
		animation.setDuration(1000);
		GameResult.startAnimation(animation);

		Animation animation1 = new AlphaAnimation(0.0f, 1.0f);
		animation1.setDuration(500);
		animation1.setStartOffset(1500);
		result_img.startAnimation(animation1);
		
		
		Animation animation2 = new AlphaAnimation(0.0f, 1.0f);
		animation2.setDuration(500);
		animation2.setStartOffset(2000);
		score_row.startAnimation(animation2);
		
		Animation animation3 = new AlphaAnimation(0.0f, 1.0f);
		animation3.setDuration(500);
		animation3.setStartOffset(2500);
		repeat_row.startAnimation(animation3);
		
		Animation animation4 = new AlphaAnimation(0.0f, 1.0f);
		animation4.setDuration(500);
		animation4.setStartOffset(3000);
		wrong_row.startAnimation(animation4);

		Animation animation5 = new AlphaAnimation(0.0f, 1.0f);
		animation5.setDuration(500);
		animation5.setStartOffset(3500);
		time_row.startAnimation(animation5);
		
		
	}
	
	
	ImageView lightening;
	
	int count = 0;
	
	public void lighteningblinking()
	{
		count++;
		lightening.setVisibility(View.VISIBLE);
		Animation animation = new AlphaAnimation(0.0f, 1.0f);
		animation.setDuration(100);
		
		final Animation animation1 = new AlphaAnimation(0.0f, 0.0f);
		animation1.setDuration(2000);
		
		final Animation animation2 = new AlphaAnimation(0.0f, 1.0f);
		animation2.setDuration(100);
		
		final Animation animation3 = new AlphaAnimation(0.0f, 0.0f);
		animation3.setDuration(2000);

		final Animation animation4 = new AlphaAnimation(0.0f, 1.0f);
		animation4.setDuration(2000);
		
		lightening.startAnimation(animation);
		
		
		 //animation1 AnimationListener
	    animation.setAnimationListener(new AnimationListener(){

	        @Override
	        public void onAnimationEnd(Animation arg0) {
	            // start animation2 when animation1 ends (continue)
	        	lightening.startAnimation(animation1);
	        }

	        @Override
	        public void onAnimationRepeat(Animation arg0) {
	            // TODO Auto-generated method stub

	        }

	        @Override
	        public void onAnimationStart(Animation arg0) {
	            // TODO Auto-generated method stub

	        }

	    });
	    
	    //animation1 AnimationListener
	    animation1.setAnimationListener(new AnimationListener(){

	        @Override
	        public void onAnimationEnd(Animation arg0) {
	            // start animation2 when animation1 ends (continue)
	        	lightening.startAnimation(animation2);
	        }

	        @Override
	        public void onAnimationRepeat(Animation arg0) {
	            // TODO Auto-generated method stub

	        }

	        @Override
	        public void onAnimationStart(Animation arg0) {
	            // TODO Auto-generated method stub

	        }

	    });
	    
	  //animation1 AnimationListener
	    animation2.setAnimationListener(new AnimationListener(){

	        @Override
	        public void onAnimationEnd(Animation arg0) {
	            // start animation2 when animation1 ends (continue)
	        	lightening.startAnimation(animation3);
	        }

	        @Override
	        public void onAnimationRepeat(Animation arg0) {
	            // TODO Auto-generated method stub

	        }

	        @Override
	        public void onAnimationStart(Animation arg0) {
	            // TODO Auto-generated method stub
	        }
	    });
	    
	  //animation1 AnimationListener
	    animation3.setAnimationListener(new AnimationListener(){

	        @Override
	        public void onAnimationEnd(Animation arg0) {
	            // start animation2 when animation1 ends (continue)
	        	lightening.startAnimation(animation4);
	        }

	        @Override
	        public void onAnimationRepeat(Animation arg0) {
	            // TODO Auto-generated method stub

	        }

	        @Override
	        public void onAnimationStart(Animation arg0) {
	            // TODO Auto-generated method stub

	        }

	    });
	}
	
	ImageView star;
	ImageView star1;
	ImageView star2;
	ImageView star3;
	ImageView star4;
	ImageView star5;
	ImageView star6;
	
	
	public void transitionstar()
	{
		this.startingView = (RelativeLayout) findViewById(R.id.startingView);
		
		star.setImageResource(R.drawable.star);
		star1.setImageResource(R.drawable.star);
		star2.setImageResource(R.drawable.star);
		star3.setImageResource(R.drawable.star);
		star4.setImageResource(R.drawable.star);
		star5.setImageResource(R.drawable.star);
		
		star.setVisibility(View.VISIBLE);
		star1.setVisibility(View.VISIBLE);
		star2.setVisibility(View.VISIBLE);
		star3.setVisibility(View.VISIBLE);
		star4.setVisibility(View.VISIBLE);
		star5.setVisibility(View.VISIBLE);

		int x = (startingView.getWidth() - star.getWidth()) / 2;
		int y = (startingView.getHeight() - star.getHeight()) / 2;

		Animation scale = new ScaleAnimation(1, 1, 1, 1, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
		// 1 second duration
		scale.setDuration(1000);
		
		RotateAnimation rotate = new RotateAnimation(0, -90, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
		rotate.setDuration(500);
		RotateAnimation rotate1 = new RotateAnimation(0, 90, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
		rotate1.setDuration(500);
		RotateAnimation rotate2 = new RotateAnimation(0, 90, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
		rotate2.setDuration(500);
		RotateAnimation rotate3 = new RotateAnimation(90, -90, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
		rotate3.setDuration(500);
		RotateAnimation rotate4 = new RotateAnimation(90, 0, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
		rotate4.setDuration(500);
		RotateAnimation rotate5 = new RotateAnimation(0, 90, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
		rotate5.setDuration(500);
		
		// Moving up
		Animation tralslate = new TranslateAnimation(x+3,-800, y+3, -800);	
		tralslate.setDuration(500);
		Animation tralslate1 = new TranslateAnimation(x+5,x +  600, y+5, -600);	
		tralslate1.setDuration(500);
		Animation tralslate2 = new TranslateAnimation(x-3, x+ 400,  y-3,y + 400);	
		tralslate2.setDuration(500);
		Animation tralslate3 = new TranslateAnimation(x-5,  -200, y-5,y +  200);	
		tralslate3.setDuration(500);
		Animation tralslate4 = new TranslateAnimation( x-3, x + 900,  y+5,  -300);	
		tralslate4.setDuration(500);
		Animation tralslate5 = new TranslateAnimation( x+3,x+ -200,y-5, y + 500);	
		tralslate5.setDuration(500);
	
		AnimationSet animSet = new AnimationSet(true);
		//animSet.setFillEnabled(true);
		animSet.setFillEnabled(true);
		animSet.setFillAfter(true);
		animSet.addAnimation(scale);
		animSet.addAnimation(rotate);
		animSet.addAnimation(tralslate);
		animSet.setDuration(500);
		star.startAnimation(animSet);
		
		AnimationSet animSet1 = new AnimationSet(true);
		//animSet1.setFillEnabled(true);
		animSet1.setFillEnabled(true);
		animSet1.setFillAfter(true);
		animSet1.addAnimation(scale);
		animSet1.addAnimation(rotate1);
		animSet1.addAnimation(tralslate1);
		animSet1.setDuration(500);
		star1.startAnimation(animSet1);
		
		AnimationSet animSet2 = new AnimationSet(true);
		//animSet2.setFillEnabled(true);
		animSet2.setFillEnabled(true);
		animSet2.setFillAfter(true);
		animSet2.addAnimation(scale);
		animSet2.addAnimation(rotate2);
		animSet2.addAnimation(tralslate2);
		animSet2.setDuration(500);
		star2.startAnimation(animSet2);
		
		AnimationSet animSet3 = new AnimationSet(true);
		//animSet3.setFillEnabled(true);
		animSet3.setFillEnabled(true);
		animSet3.setFillAfter(true);
		animSet3.addAnimation(scale);
		animSet3.addAnimation(rotate3);
		animSet3.addAnimation(tralslate3);
		animSet3.setDuration(500);
		star3.startAnimation(animSet3);
		
		AnimationSet animSet4 = new AnimationSet(true);
		//animSet4.setFillEnabled(true);
		animSet4.setFillEnabled(true);
		animSet4.setFillAfter(true);
		animSet4.addAnimation(scale);
		animSet4.addAnimation(rotate4);
		animSet4.addAnimation(tralslate4);
		animSet4.setDuration(500);
		star4.startAnimation(animSet4);
	
		AnimationSet animSet5 = new AnimationSet(true);
		//animSet5.setFillEnabled(true);
		animSet5.setFillEnabled(true);
		animSet5.setFillAfter(true);
		animSet5.addAnimation(scale);
		animSet5.addAnimation(rotate5);
		animSet5.addAnimation(tralslate5);
		animSet5.setDuration(500);
		star5.startAnimation(animSet5);
	
	}
	
	public boolean onTouchEvent(MotionEvent event) {
		if(event.getAction() == MotionEvent.ACTION_DOWN) {
 			eX = event.getX(); // 터치 시작지점 x좌표 저장		
// 			imgStartView.get
 		}
		return true;
	}
	
	
	
	
	private class GameEndWebService extends AsyncTask<String, Void, String>{

		@Override
		protected String doInBackground(String... arg0) {
			// TODO Auto-generated method stub
			
			HashMap<String, String> params = new HashMap<String, String>();
			
			
			if(StartInGame.this.GameState == STATE_END)
			{
				if(Preferences.cur_ChallengeList.GameType == TYPE_SINGLEGAME)
				{
					
					params.clear();
					
					params.put("MatchCompTime", String.valueOf(Preferences.cur_ChallengeList.fplayer_comp_time));
					params.put("MatchResult", String.valueOf(Preferences.cur_ChallengeList.result));
					params.put("FBID", Preferences.FBID);

					String result = HttpManager.postHttpResponse(URI.create(Preferences.insertSingleMatchDetails), params);
				}
				else
				{
					if(Preferences.cur_ChallengeList.GameType == TYPE_ACCEPTGAME)
					{
						params.clear();
						/*
						params.put("Score", String.valueOf(Preferences.Score));
						params.put("FBID", Preferences.FBID);

						String result = HttpManager.postHttpResponse(URI.create(Preferences.updateUserScore), params);
						*/
						String result = "";
						params.clear();
						params.put("FBID", Preferences.FBID);
						params.put("Message", StartInGame.this.editTextToFriend.getText().toString());
						params.put("CompTime", String.valueOf(Preferences.cur_ChallengeList.splayer_comp_time));
						params.put("CompPoss", String.valueOf(Preferences.cur_ChallengeList.splayer_comp_poss));
//						params.put("MatchResult", String.valueOf(Preferences.cur_ChallengeList.result));
						params.put("MatchResult", String.valueOf(0));
						params.put("WrongAttemp", String.valueOf(Preferences.cur_ChallengeList.splayer_wrong_tried));
						params.put("MatchID", Preferences.cur_ChallengeList.match_id);
						params.put("Repeat", String.valueOf(Preferences.cur_ChallengeList.splayer_repeat));
						params.put("WinnerID", Preferences.cur_ChallengeList.WinnerID);
						
						result = HttpManager.postHttpResponse(URI.create(Preferences.insertSPlayerDetails), params);
						
					}
					else
					{
						params.clear();
						params.put("FBID", Preferences.FBID);
						params.put("Message", StartInGame.this.editTextToFriend.getText().toString());
						params.put("CompTime", String.valueOf(Preferences.cur_ChallengeList.fplayer_comp_time));
						params.put("CompPoss", String.valueOf(Preferences.cur_ChallengeList.fplayer_comp_poss));
						params.put("MatchResult", String.valueOf(0));
						params.put("WrongAttemp", String.valueOf(Preferences.cur_ChallengeList.fplayer_wrong_tried));
						params.put("GID", Preferences.cur_ChallengeList.gameplay_id);
						params.put("FUserName", Preferences.UserName);
						params.put("Repeat", String.valueOf(Preferences.cur_ChallengeList.fplayer_repeat));
						params.put("SPlayerID", Preferences.cur_ChallengeList.splayer_id);
						params.put("SPlayerUserName", Preferences.cur_ChallengeList.splayer_username);
						
						String result = HttpManager.postHttpResponse(URI.create(Preferences.insertFPlayerDetails), params);
					}
				}
			}
			return null;
		}
		
	    @Override
	    protected void onPostExecute(String result) {               
	    	
	    	Intent intent = new Intent(StartInGame.this, MadMathz.class);
	    	startActivity(intent);
//	    	StartInGame.this.overridePendingTransition(R.anim.slideout, 0);
	    	isContinue = true;
	    	StartInGame.this.finish();
	    }
	}
	
	
	
	private class GameStartWebService extends AsyncTask<String, Void, String> {
		
		public Bundle game_info = new Bundle();
		public String gameplay_response = "";

	    @Override
	    protected String doInBackground(String... params) {
	        	//Http Request
	    	try{
		    	if(Preferences.cur_ChallengeList.GameType == TYPE_SINGLEGAME || Preferences.cur_ChallengeList.GameType == TYPE_CHALLENGEGAME) //single game
		    	{
		    		HashMap<String, String> param = new HashMap<String, String>();
		    		param.put("GPID", String.valueOf(0));
		    		param.put("Level", String.valueOf(level));
		        	gameplay_response = HttpManager.postHttpResponse(URI.create(Preferences.selectSingleGameRandomly_v2), param);
		    	}
		    	
		    	if(Preferences.cur_ChallengeList.GameType == TYPE_ACCEPTGAME)
		    	{
		    		HashMap<String, String> param = new HashMap<String, String>();
		    		param.put("GPID", Preferences.cur_ChallengeList.gameplay_id);
		    		gameplay_response = HttpManager.postHttpResponse(URI.create(Preferences.selectGamePlayByID), param);
		    	}
		    	
		    	if(Preferences.cur_ChallengeList.GameType == TYPE_RECHALLENGE)
		    	{
		    		HashMap<String, String> param = new HashMap<String, String>();
		    		param.put("FID", Preferences.FBID);
		    		param.put("SID", Preferences.cur_ChallengeList.splayer_id);
		    		String res = HttpManager.postHttpResponse(URI.create(Preferences.getTotalMatchResult), param);
		    		
		    		param.clear();		    		
		    		param.put("GPID", String.valueOf(0));
		    		param.put("Level", String.valueOf(level));
		        	gameplay_response = HttpManager.postHttpResponse(URI.create(Preferences.selectSingleGameRandomly_v2), param);


	    	}
		    	
		    	if(gameplay_response != null)
		    	{
		    		JSONObject json = new JSONObject(gameplay_response).getJSONArray("posts").getJSONObject(0).getJSONObject("game_play");
		    		Preferences.cur_ChallengeList.gameplay_id = json.getString("id");
		    		int num[] = new int[16];
		    		for(int i = 1; i <= 16; i++)
		    		{
		    			num[i -1] = json.getInt("img_number" + i);
		    		}
		    		
		    		Preferences.cur_ChallengeList.Tiles = num;
		    		Preferences.cur_ChallengeList.answer = json.getInt("answer");
		    		Preferences.cur_ChallengeList.possibility = json.getInt("total_possibility");
		    		Preferences.cur_ChallengeList.gameplay_id = json.getString("id");
		    	}
	    	}catch(Exception e)
	    	{
	    		
	    	}
	        return "Executed";
	    }      

	    @Override
	    protected void onPostExecute(String result) {               
			stopService(new Intent(StartInGame.this, MediaPlayerService.class));

	    	Intent intent = new Intent(StartInGame.this, GameView.class);
	    	startActivity(intent);
//	    	StartInGame.this.overridePendingTransition(0,  R.anim.slidein);
	    	isContinue = true;
	    	StartInGame.this.finish();
	    }
	}
	
	
	
	
	
	
	RelativeLayout loadingView;
	public int level = 0;
	
	public void onClick(View v) {
		// TODO Auto-generated method stub
		//level select
		if(Preferences.isIncomingCall) return;
		RelativeLayout level_select = (RelativeLayout)findViewById(R.id.level_select);
		if(v.getId() == R.id.level_easy_btn)
		{
			level = 1;
			level_select.setVisibility(View.GONE);
			bDrawLevel = false;
		}
		if(v.getId() == R.id.level_middle_btn)
		{
			level = 2;
			level_select.setVisibility(View.GONE);
			bDrawLevel = false;
		}
		if(v.getId() == R.id.level_hard_btn)
		{
			level = 3;
			level_select.setVisibility(View.GONE);
			bDrawLevel = false;
		}
		
		if(v.getId() == R.id.level_hard_btn || v.getId() == R.id.level_middle_btn ||
				v.getId() == R.id.level_easy_btn)
		{
			drawMessage();

		}
		
		if(bDrawLevel == true) return;
		
		if(v.getId() == R.id.startingView){

			if(GameState == STATE_START)
			{
				mHandler.post(new Runnable(){

					@Override
					public void run() {
						// TODO Auto-generated method stub
						Utility.drawLoading(loadingView, StartInGame.this);
					}});
				
				mHandler.post(new Runnable(){
	
					@Override
					public void run() {
						// TODO Auto-generated method stub
						new GameStartWebService().execute("");
	
					}
					
				});
			}
		}
		
		if(v.getId() == R.id.imgHome || v.getId() == R.id.mBtnSubmit)
		{
			if(Preferences.bBGM){
				startService(new Intent(StartInGame.this, MediaPlayerService.class));
			}
			
			loadingView.setVisibility(View.VISIBLE);
			
			mHandler.post(new Runnable(){

				@Override
				public void run() {
					// TODO Auto-generated method stub
					Utility.drawLoading(loadingView, StartInGame.this);
				}});

			mHandler.post(new Runnable(){

				@Override
				public void run() {
					// TODO Auto-generated method stub
					new GameEndWebService().execute("");
				}
				
			});
			

		}
	}
	/*
	public void onDestroy()
	{
		stopService(new Intent(this, MediaPlayerService.class));
		
		if(this.sfLose.isPlaying())
		{
			this.sfLose.stop();
		}
		if(this.sfWin.isPlaying())
		{
			this.sfWin.stop();
		}
		
		super.onDestroy();
		
	}
	*/
	/*
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		
		if(keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_HOME || keyCode == KeyEvent.KEYCODE_MENU)

		{
			stopService(new Intent(this, MediaPlayerService.class));
		}
		if(keyCode == KeyEvent.KEYCODE_ENTER)
		{
			
			if(Preferences.cur_ChallengeList.GameType != TYPE_SINGLEGAME)
			{
				
			}
		}
		
//		GCMRegistrar.unregister(getApplicationContext());
//		GCMRegistrar.onDestroy(getApplicationContext());
		return super.onKeyDown(keyCode, event);	
	}
	*/
	
	public boolean isContinue = false;
	
	protected void onStop()
	{
		super.onStop();
		if(Preferences.isIncomingCall)
		{
			stopService(new Intent(this, MediaPlayerService.class));
		}else	if(!isContinue)
		{
			stopService(new Intent(this, MediaPlayerService.class));
			this.finish();
		}
		
		
	}
	
	protected void onResume()
	{
		super.onResume();
		
		if(Preferences.isIncomingCall)
		{
			Preferences.isIncomingCall = false;
			if(Preferences.bBGM) startService(new Intent(this, MediaPlayerService.class));
			mHandler.post(new Runnable(){ //call process

				@Override
				public void run() {
					// TODO Auto-generated method stub
					
			    	LayoutInflater mInflater = (LayoutInflater) StartInGame.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			    	View inflatedView = mInflater.inflate(R.layout.phone_call, null);
			    	
			    	final RelativeLayout loadingView = (RelativeLayout)findViewById(R.id.LoadingView);
			    	loadingView.setVisibility(View.VISIBLE);
			    	loadingView.setBackgroundColor(0xB2000000);
			    	loadingView.removeAllViews();
			    	
			    	loadingView.addView(inflatedView);
			    	
			    	ImageButton btn_resume = (ImageButton)inflatedView.findViewById(R.id.btn_resume);
			    	btn_resume.setOnTouchListener(new ButtonHighlight(btn_resume));
			    	btn_resume.setOnClickListener(new OnClickListener(){

						@Override
						public void onClick(View arg0) {
							// TODO Auto-generated method stub
							loadingView.setVisibility(View.INVISIBLE);
							loadingView.removeAllViews();
							Preferences.isIncomingCall = false;

						}});
			    	
			    	ImageButton btn_quit = (ImageButton)inflatedView.findViewById(R.id.btn_quit);
			    	btn_quit.setOnTouchListener(new ButtonHighlight(btn_quit));
			    	
			    	btn_quit.setOnClickListener(new OnClickListener(){

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							stopService(new Intent(StartInGame.this, MediaPlayerService.class));
							StartInGame.this.finish();
							Preferences.isIncomingCall = false;

							
						}});
				}});
		}
		
		
	}
	
}
