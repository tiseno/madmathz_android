package com.facebook.madmathz;
/**
 * Copyright 2012 Facebook
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.facebook.madmathz.connector.BaseDialogListener;

public class FriendsList extends Activity implements OnItemClickListener, OnClickListener {
    private Handler mHandler;

    protected ListView friendsList;
    protected static JSONArray jsonArray;
    protected String graph_or_fql = "graph";
    ImageButton friend_home_btn;
    
    public boolean isContinue = false;
    

    /*
     * Layout the friends' list
     */
    
    public HashMap<String, String> userData = new HashMap<String, String>();  //fb_id, score
    public EditText searchTxt;
    
    class friend_item
    {
    	String id;
    	String picture;
    	String name;
    }
    
    ArrayList<friend_item> main_friend_arr = new ArrayList<friend_item>();
    ArrayList<friend_item> tmp_friend_arr = new ArrayList<friend_item>();
    
    

    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        mHandler = new Handler();
        setContentView(R.layout.activity_friendslist);

        Bundle extras = getIntent().getExtras();
        String apiResponse = extras.getString("API_RESPONSE");
        String AllUserResponse = extras.getString("ALL_USERS");
        friend_home_btn = (ImageButton)findViewById(R.id.friend_home_btn);
        friend_home_btn.setOnClickListener(this);
        friend_home_btn.setOnTouchListener(new ButtonHighlight(friend_home_btn));
        
        //parse all user info json
        try
        {
        	JSONArray  json = new JSONObject(AllUserResponse).getJSONArray("posts");
        	
        	if(json != null)
        	{
        		for(int i = 0; i < json.length(); i++)
        		{
        			String fb_id = json.getJSONObject(i).getJSONObject("user").getString("fb_id");
        			
        			if(fb_id.equals("")) continue;
        			
        			String score = json.getJSONObject(i).getJSONObject("user").getString("score");
        			
        			userData.put(fb_id, score);
        			
        		}
        	}
        	
        }
        catch(Exception e)
        {
        	
        }
        //graph_or_fql = extras.getString("METHOD");
        try {
            if (graph_or_fql.equals("graph")) {
                jsonArray = new JSONObject(apiResponse).getJSONArray("data");
            } else {
                jsonArray = new JSONArray(apiResponse);
            }
        } catch (JSONException e) {
            showToast("Error: " + e.getMessage());
            return;
        }
        
        //parse friend_list
        main_friend_arr.clear();
        
        for(int i = 0; i < jsonArray.length(); i++)
        {
        	try {
        		friend_item item = new friend_item();
				item.id = jsonArray.getJSONObject(i).getString("id");
				item.name = jsonArray.getJSONObject(i).getString("name");
	   			item.picture = jsonArray.getJSONObject(i).getJSONObject("picture").getJSONObject("data").getString("url");
	   			main_friend_arr.add(item);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        tmp_friend_arr = main_friend_arr;
        
        
        friendsList = (ListView) findViewById(R.id.friendlist);
        
        View header = getLayoutInflater().inflate(R.layout.friendlist_header, null);
        //View footer = getLayoutInflater().inflate(R.layout.footer, null);
        friendsList.addHeaderView(header);
        friendsList.setHeaderDividersEnabled(true);
        
        
        //friendsList.setOnItemClickListener(this);
        final FriendListAdapter friendsAdapter = new FriendListAdapter(this, R.layout.friend_item, main_friend_arr);
        friendsList.setAdapter(friendsAdapter);
        searchTxt = (EditText)header.findViewById(R.id.searchTxt);

        friendsList.setTextFilterEnabled(true);
        searchTxt.addTextChangedListener(new TextWatcher()
       {


           @Override
           public void onTextChanged( CharSequence arg0, int arg1, int arg2, int arg3)
           {
               // TODO Auto-generated method stub
          	 friendsAdapter.getFilter().filter(arg0);

           }



           @Override
           public void beforeTextChanged( CharSequence arg0, int arg1, int arg2, int arg3)
           {
               // TODO Auto-generated method stub

           }



           @Override
           public void afterTextChanged( Editable arg0)
           {
               // TODO Auto-generated method stub

           }
       });

//        showToast(getString(R.string.can_post_on_wall));
    }

    /*
     * Clicking on a friend should popup a dialog for user to post on friend's
     * wall.
     */
    @Override
    @SuppressWarnings("deprecation")
    public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3) {
    	/*
            final long friendId;
            if (graph_or_fql.equals("graph")) {
                friendId = Long.getLong(tmp_friend_arr.get(position).id); //jsonArray.getJSONObject(position).getLong("id");
            }
            String name = tmp_friend_arr.get(position).name; 	//jsonArray.getJSONObject(position).getString("name");
            */
    }

    /*
     * Callback after the message has been posted on friend's wall.
     */
    public class PostDialogListener extends BaseDialogListener {
        @Override
        public void onComplete(Bundle values) {
            final String postId = values.getString("post_id");
            if (postId != null) {
                showToast("Message posted on the wall.");
            } else {
                showToast("No message posted on the wall.");
            }
        }
    }

    public void showToast(final String msg) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Toast toast = Toast.makeText(FriendsList.this, msg, Toast.LENGTH_LONG);
                toast.show();
            }
        });
    }
    
    public void FriendListClickListener(View v)
    {
    	if(Preferences.isIncomingCall) return;
    	int position = friendsList.getPositionForView(v);
    	if(position > 0) position--;
    	final String friendId;
    	String name = "";
   		 //friendId = jsonArray.getJSONObject(position).getString("id");
		 //name = jsonArray.getJSONObject(position).getString("name");
		
		friendId = tmp_friend_arr.get(position).id;
		name = tmp_friend_arr.get(position).name;
   		   

	       	if(!userData.containsKey(friendId))
	       	{
	       		
	       		new AlertDialog.Builder(this).setTitle("Post On Wall")
	               .setMessage(String.format("%s post on wall", name))
	               .setPositiveButton("Post", new DialogInterface.OnClickListener() {
	                   @Override
	                   public void onClick(DialogInterface dialog, int which) {
	                   	Bundle bundle = new Bundle();
	               		bundle.putString("link", "www.facebook.com/Madmathz");
	               		bundle.putString("picture", "http://zrlim.com/mm/logo.png");
	               		bundle.putString("name", "MadMathz");
	               		bundle.putString("caption", "Game Descriptions");
	               		bundle.putString("description", "This game will test your basic mathematic. You will be given a set of numbers and provided with answer then you need to find combination of number to match the answer within ONE minute. It is either you are loser or winner.");
	               		bundle.putString("to", friendId);
	               		bundle.putString("app_id", getResources().getString(R.string.app_id));
	                       Utility.mFacebook.dialog(FriendsList.this, "feed", bundle,
	                               new PostDialogListener());
	                   }

	               }).setNegativeButton("Cancel", null).show();
	               

	       	}
	       	else
	   		{
	       		//userinfo init
	       		ChallengeList t_user = new ChallengeList();
	       		t_user.fplayer_id = Preferences.FBID;
	       		t_user.fplayer_username = Preferences.UserName;
	       		t_user.splayer_id = friendId;
	       		t_user.splayer_username = name;
	       		t_user.GameType = StartInGame.TYPE_CHALLENGEGAME;
	       		Preferences.cur_ChallengeList = t_user;
	       		
	           	Intent intent = new Intent(FriendsList.this, StartInGame.class);
	           	intent.putExtra("GameState", StartInGame.STATE_START);
	           	startActivity(intent);
	           	isContinue = true;
	           	this.finish();
	   		}
   
    }


    /**
     * Definition of the list adapter
     */
//    public class FriendListAdapter extends BaseAdapter {
    public class FriendListAdapter extends ArrayAdapter implements Filterable{
    	private LayoutInflater mInflater;
        FriendsList friendsList;
        
        /*
        public FriendListAdapter(FriendsList friendsList) {
            this.friendsList = friendsList;
            if (Utility.model == null) {
                Utility.model = new FriendsGetProfilePics();
            }
            Utility.model.setListener(this);
            mInflater = LayoutInflater.from(friendsList.getBaseContext());
        }
        */
        public FriendListAdapter(Context context, int textViewResourceId, ArrayList<friend_item> results)
        {
        	super(context,textViewResourceId, results);
        	
        	this.friendsList = (FriendsList)context;
            if (Utility.model == null) {
                Utility.model = new FriendsGetProfilePics();
            }
            Utility.model.setListener(this);
            mInflater = LayoutInflater.from(friendsList.getBaseContext());
        	
        }

        @Override
        public int getCount() {
           // return jsonArray.length();
        	return tmp_friend_arr.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }
        

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View hView = convertView;
            if (convertView == null) {
                hView = mInflater.inflate(R.layout.friend_item, null);
                ViewHolder holder = new ViewHolder();
                holder.profile_pic = (ImageView) hView.findViewById(R.id.profile_pic);
                holder.name = (TextView) hView.findViewById(R.id.name);
                holder.name.setTextColor(Preferences.tx_color);
                holder.name.setTypeface(Preferences.tf_normal, Typeface.BOLD);
                
                holder.info = (TextView) hView.findViewById(R.id.info);
                holder.info.setTextColor(Preferences.tx_color);
                holder.info.setTypeface(Preferences.tf_normal);
                holder.info.setTextSize(11);
                
                holder.btn = (ImageButton)hView.findViewById(R.id.FriendBtn);
                holder.btn.setOnTouchListener(new ButtonHighlight(holder.btn));
            
                hView.setTag(holder);
            }

            ViewHolder holder = (ViewHolder) hView.getTag();
            if (graph_or_fql.equals("graph")) {
                holder.profile_pic.setImageBitmap(Utility.model.getImage(
                       tmp_friend_arr.get(position).id, tmp_friend_arr.get(position).picture));
            }
            holder.name.setText(tmp_friend_arr.get(position).name);
            if (graph_or_fql.equals("graph")) {
            	String fb_id =  tmp_friend_arr.get(position).id; //jsonObject.getString("id");
            	if(userData.containsKey(fb_id))
            	{
            		String user_score = userData.get(fb_id) + " points";
                	holder.info.setText(user_score);
                	
                	//btn chanllenge
                	//holder.btn.setBackgroundResource(R.drawable.btn_challenge);
                	holder.btn.setImageResource(R.drawable.btn_challenge);
                	holder.btn.setOnTouchListener(new ButtonHighlight(holder.btn));
            	}
            	else
            	{
            		holder.info.setText("");
            		//holder.btn.setBackgroundResource(R.drawable.btn_invite);
            		holder.btn.setImageResource(R.drawable.btn_invite);
                	holder.btn.setOnTouchListener(new ButtonHighlight(holder.btn));

            	}
            	
            } else {
            }

            return hView;
        }
        
        private PTypeFilter filter;
        
        @Override
        public Filter getFilter() {
            Log.i("CustomListAdapter", "testing filter");
            if (filter == null){
              filter  = new PTypeFilter();
            }
            return filter;
          }
        

    private class PTypeFilter extends Filter{


      @SuppressWarnings("unchecked")
      @Override
      protected void publishResults(CharSequence prefix,
                                    FilterResults results) {
        // NOTE: this function is *always* called from the UI thread.
         tmp_friend_arr =  (ArrayList<friend_item>)results.values;
         notifyDataSetChanged();
      }

      @SuppressWarnings("unchecked")
      protected FilterResults performFiltering(CharSequence prefix) {
            // NOTE: this function is *always* called from a background thread, and
            // not the UI thread. 

            FilterResults results = new FilterResults();
            ArrayList<friend_item> i = new ArrayList<friend_item>();
            if (prefix!= null && prefix.toString().length() > 0) {
                // use the initial values !!! 
                for (int index = 0; index < main_friend_arr.size(); index++) {
                    friend_item si = main_friend_arr.get(index);
                    int length = prefix.length();
                    int t_length = si.name.length();
                    if(length > t_length) length = t_length;
                    // if you compare the Strings like you did it will never work as you compare the full item string(you'll have a match only when you write the EXACT word)
                    // keep in mind that you take in consideration capital letters!
                    if(si.name.toLowerCase().substring(0, length).compareTo(prefix.toString().toLowerCase()) == 0){
                      i.add(si);  
                    }
                }
                results.values = i;
                results.count = i.size();                   
            }
            else{
                synchronized (main_friend_arr){
                    results.values = main_friend_arr;
                    results.count = main_friend_arr.size();
                }
            }

            return results;
         }
       } 
        
    }

    class ViewHolder {
        ImageView profile_pic;
        TextView name;
        TextView info;
        ImageButton btn;
        int fb_id;
    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(Preferences.isIncomingCall) return;
		if(v.getId() == R.id.friend_home_btn)
		{
			Intent intent = new Intent(this, MadMathz.class);
			startActivity(intent);
			isContinue = true;
			this.finish();
		}
	}
	
	protected void onStop()
	{
		super.onStop();

		if(Preferences.isIncomingCall) //phone call
		{
			stopService(new Intent(this, MediaPlayerService.class));
		} else if(!isContinue) //unexpected down
		{
			stopService(new Intent(this, MediaPlayerService.class));
			Preferences.savePreferences(getApplicationContext());
			this.finish();
		}
		
	}
	

	
	protected void onResume()
	{
		super.onResume();

		if(Preferences.isIncomingCall)
		{
			if(Preferences.bBGM) startService(new Intent(this, MediaPlayerService.class));
			
			mHandler.post(new Runnable(){ //call process

				@Override
				public void run() {
					// TODO Auto-generated method stub
					
			    	LayoutInflater mInflater = (LayoutInflater) FriendsList.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			    	View inflatedView = mInflater.inflate(R.layout.phone_call, null);
			    	inflatedView.setBackgroundColor(0xB2000000);
			    	
			    	final RelativeLayout loadingView = (RelativeLayout)findViewById(R.id.loadingView);
			    	loadingView.setVisibility(View.VISIBLE);
			    	loadingView.removeAllViews();
			    	
			    	loadingView.addView(inflatedView);
			    	
			    	ImageButton btn_resume = (ImageButton)inflatedView.findViewById(R.id.btn_resume);
			    	btn_resume.setOnTouchListener(new ButtonHighlight(btn_resume));
			    	btn_resume.setOnClickListener(new OnClickListener(){

						@Override
						public void onClick(View arg0) {
							// TODO Auto-generated method stub
							loadingView.removeAllViews();
							loadingView.setVisibility(View.INVISIBLE);
							Preferences.isIncomingCall = false;

						}});
			    	
			    	ImageButton btn_quit = (ImageButton)inflatedView.findViewById(R.id.btn_quit);
			    	btn_quit.setOnTouchListener(new ButtonHighlight(btn_quit));
			    	
			    	btn_quit.setOnClickListener(new OnClickListener(){

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							stopService(new Intent(FriendsList.this, MediaPlayerService.class));
							FriendsList.this.finish();
							Preferences.isIncomingCall = false;

						}});
				}});
		}
	}
}
