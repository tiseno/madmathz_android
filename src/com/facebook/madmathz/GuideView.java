package com.facebook.madmathz;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ViewFlipper;

public class GuideView extends Activity implements View.OnTouchListener, OnClickListener {
	ImageView imgGuideView;
	float xAtDown;
	float xAtUp;
	int pageNo = 1;
	ImageButton mBtnSkip;
	ViewFlipper page;
   Animation animFlipInForeward;
   Animation animFlipOutForeward;
   Animation animFlipInBackward;
   Animation animFlipOutBackward;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_guide_view);
		//this.imgGuideView = (ImageView)findViewById(R.id.imgGuide);
		//this.imgGuideView.setImageResource(R.drawable.guide_1);
		//this.imgGuideView.setOnTouchListener(onTouch);
		this.mBtnSkip = (ImageButton)findViewById(R.id.mBtnSkip);
		this.mBtnSkip.setOnClickListener(this);
		this.mBtnSkip.setOnTouchListener(new ButtonHighlight(mBtnSkip));
		page = (ViewFlipper)findViewById(R.id.flipper);
		animFlipInBackward = AnimationUtils.loadAnimation(this, R.anim.slide_left_in);
		animFlipOutBackward = AnimationUtils.loadAnimation(this, R.anim.slide_left_out);
		animFlipInForeward = AnimationUtils.loadAnimation(this, R.anim.slide_right_in);
		animFlipOutForeward = AnimationUtils.loadAnimation(this, R.anim.slide_right_out);
	}
	
	private void SwipeRight(){
		page.setInAnimation(animFlipInBackward);
		page.setOutAnimation(animFlipOutBackward);
		page.showPrevious();
	}
		  
	private void SwipeLeft(){
		page.setInAnimation(animFlipInForeward);
		page.setOutAnimation(animFlipOutForeward);
		page.showNext();
	}
	 @Override
	 public boolean onTouchEvent(MotionEvent event) {
	  // TODO Auto-generated method stub
	     return gestureDetector.onTouchEvent(event);
	 }

	 SimpleOnGestureListener simpleOnGestureListener
	    = new SimpleOnGestureListener(){

	  @Override
	  public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
	    float velocityY) {

	   float sensitvity = 50;
	   if((e1.getX() - e2.getX()) > sensitvity){
	    SwipeLeft();
	   }else if((e2.getX() - e1.getX()) > sensitvity){
	    SwipeRight();
	   }
	  
	   return true;
	  }
	    
	    };
	   
	    GestureDetector gestureDetector
	 = new GestureDetector(simpleOnGestureListener);
		  
	
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_guide_view, menu);
		return true;
	}
	
	/*
	public OnTouchListener onTouch = new OnTouchListener(){
		
		public boolean onTouch(View v, MotionEvent event) {
			
			if(v.getId()==R.id.imgGuide){
				if(event.getAction() == MotionEvent.ACTION_DOWN) {
	     			xAtDown = event.getX(); // 터치 시작지점 x좌표 저장			
	     		}
				else if(event.getAction() == MotionEvent.ACTION_UP){
	     			xAtUp = event.getX(); 	// 터치 끝난지점 x좌표 저장
	     			if( (xAtUp - xAtDown) > 50  ) {
	     				if(pageNo > 1){
	     					pageNo--;
	     				}
	     			}
	     			if( (xAtDown - xAtUp) > 50  ) {
	     				if(pageNo < 5){
	     					pageNo++;
	     				}
	     			}
	     			if(pageNo == 1){
	     				imgGuideView.setImageResource(R.drawable.guide_1);
	     			}
					if(pageNo == 2){
						imgGuideView.setImageResource(R.drawable.guide_2);    				
					}
					if(pageNo == 3){
						imgGuideView.setImageResource(R.drawable.guide_3);	
					}
					if(pageNo == 4){
						imgGuideView.setImageResource(R.drawable.guide_4);	
					}
					if(pageNo == 5){
						imgGuideView.setImageResource(R.drawable.guide_5);	
					}
				}
			}
			return true;
		}
	};
	*/
	public void onClick(View v) {
		if(v.getId() == R.id.mBtnSkip)
		{
			isContinue = true;
			this.finish();
		}
	}

	@Override
	public boolean onTouch(View arg0, MotionEvent arg1) {
		// TODO Auto-generated method stub
		
		return false;
	}
	
	public boolean isContinue = false;
	
	protected void onStop()
	{
		super.onStop();
		if(Preferences.isIncomingCall)
		{
			stopService(new Intent(this, MediaPlayerService.class));
		}else if(!isContinue)
		{
			stopService(new Intent(this, MediaPlayerService.class));
			setResult(Preferences.STATE_CLOSE_APP);
			this.finish();
		}
		
	
	}
	
	Handler mHandler = new Handler();
	
	protected void onResume()
	{
		super.onResume();		

		if(Preferences.isIncomingCall)
		{
		Preferences.isIncomingCall = false;
		if(Preferences.bBGM) startService(new Intent(this, MediaPlayerService.class));
		
		mHandler.post(new Runnable(){ //call process

			@Override
			public void run() {
				// TODO Auto-generated method stub
				
		    	LayoutInflater mInflater = (LayoutInflater) GuideView.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		    	View inflatedView = mInflater.inflate(R.layout.phone_call, null);
		    	
		    	final RelativeLayout loadingView = (RelativeLayout)findViewById(R.id.loadingView);
		    	loadingView.setVisibility(View.VISIBLE);
		    	loadingView.setBackgroundColor(0xB2000000);
		    	loadingView.removeAllViews();
		    	
		    	loadingView.addView(inflatedView);
		    	
		    	ImageButton btn_resume = (ImageButton)inflatedView.findViewById(R.id.btn_resume);
		    	btn_resume.setOnTouchListener(new ButtonHighlight(btn_resume));
		    	btn_resume.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						loadingView.setVisibility(View.INVISIBLE);
						loadingView.removeAllViews();
					}});
		    	
		    	ImageButton btn_quit = (ImageButton)inflatedView.findViewById(R.id.btn_quit);
		    	btn_quit.setOnTouchListener(new ButtonHighlight(btn_quit));
		    	
		    	btn_quit.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						stopService(new Intent(GuideView.this, MediaPlayerService.class));
						GuideView.this.setResult(Preferences.STATE_CLOSE_APP);
						GuideView.this.finish();
						
					}});
			}});
		}
		
	}


	
}
