package com.facebook.madmathz;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Setting extends Activity implements  OnClickListener{
	ImageButton mBtnRet;
	ImageButton mBtnGuide;
	LinearLayout settingView;
	ImageView imgViewBGM;
	ImageView imgViewEffect;
	Bitmap imgView;
	ImageButton imgFBSignOut;
	ImageButton mBtnMoreTips;
	boolean flgBGM = false;
	boolean flgEffect = false;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_setting);
		mBtnRet = (ImageButton)findViewById(R.id.mBtnRet);
		mBtnRet.setOnClickListener(this);
		mBtnRet.setOnTouchListener(new ButtonHighlight(mBtnRet));
		mBtnGuide = (ImageButton)findViewById(R.id.mBtnGuide);
		mBtnGuide.setOnClickListener(this);
		mBtnGuide.setOnTouchListener(new ButtonHighlight(mBtnGuide));
		imgViewBGM = (ImageView)findViewById(R.id.imgViewBGM);
		imgViewBGM.setOnClickListener(this);
		imgViewEffect = (ImageView)findViewById(R.id.imgViewEffect);
		imgViewEffect.setOnClickListener(this);
		imgFBSignOut = (ImageButton)findViewById(R.id.mBtnFBSignOut);
		imgFBSignOut.setOnClickListener(this);
		imgFBSignOut.setOnTouchListener(new ButtonHighlight(imgFBSignOut));
		mBtnMoreTips = (ImageButton)findViewById(R.id.mBtnMoreTips);
		mBtnMoreTips.setOnClickListener(this);
		mBtnMoreTips.setOnTouchListener(new ButtonHighlight(mBtnMoreTips));
		
		TextView txt_background = (TextView)findViewById(R.id.background_txt);
		TextView txt_music = (TextView)findViewById(R.id.txt_music);
		TextView txt_sound = (TextView)findViewById(R.id.txt_soundeffect);
		TextView txt_guide = (TextView)findViewById(R.id.txt_guide);
		TextView txt_moretips = (TextView)findViewById(R.id.txt_moretips);
		txt_background.setTextColor(Preferences.tx_color);
		txt_background.setTypeface(Preferences.tf_adonais);
		
		txt_music.setTextColor(Preferences.tx_color);
		txt_music.setTypeface(Preferences.tf_adonais);
		
		txt_sound.setTextColor(Preferences.tx_color);
		txt_sound.setTypeface(Preferences.tf_adonais);
		
		txt_guide.setTextColor(Preferences.tx_color);
		txt_guide.setTypeface(Preferences.tf_adonais);
		txt_moretips.setTextColor(Preferences.tx_color);
		txt_moretips.setTypeface(Preferences.tf_adonais);
		
		
		if(Preferences.bBGM) imgViewBGM.setImageResource(R.drawable.btn_off);
		else imgViewBGM.setImageResource(R.drawable.btn_on);
		
		if(Preferences.bSound) imgViewEffect.setImageResource(R.drawable.btn_off);
		else imgViewEffect.setImageResource(R.drawable.btn_on);

	}
		
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_setting, menu);
		return true;
	}
	public void onClick(View v) {
		 if(mBtnRet.equals(v)){
			 //Intent intent = new Intent(this, MadMathz.class);		 
			 //intent.putExtra("ableFlag" , "able");
		     //startActivity(intent);
			 Intent intent = new Intent(this, MadMathz.class);
			 startActivity(intent);
			 //intent.putExtra("result", MadMathz.DATA_RETURN);
			 //setResult(RESULT_OK, intent);
			 isContinue = true;
		     this.finish();
		 }
		 if(mBtnGuide.equals(v)){
			 Intent intent = new Intent(this, GuideView.class);		 
			 //intent.putExtra("setImgView" , "startGameState");
		     startActivity(intent);  
		     isContinue = true;
		 }
		 
		 if(this.imgViewBGM.equals(v))
		 {
			 Preferences.bBGM = !Preferences.bBGM;
			 Preferences.savePreferences(getApplicationContext());

			 if(Preferences.bBGM)
			 {
  				startService(new Intent(Setting.this, MediaPlayerService.class));
  				imgViewBGM.setImageResource(R.drawable.btn_off);
  				imgViewBGM.invalidate();
			 }
			 else
			 {
				stopService(new Intent(Setting.this, MediaPlayerService.class));
				imgViewBGM.setImageResource(R.drawable.btn_on);
				imgViewBGM.invalidate();
			 }
		 }
		 
		 if(this.imgViewEffect.equals(v))
		 {
			 Preferences.bSound = !Preferences.bSound;
			 Preferences.savePreferences(getApplicationContext());
			 
			 if(Preferences.bSound)
			 {
				 imgViewEffect.setImageResource(R.drawable.btn_off);
				 imgViewEffect.invalidate();
			 }
			 else
			 {
				 imgViewEffect.setImageResource(R.drawable.btn_on);
				 imgViewEffect.invalidate();
			 }
		 }
		 
		 if(imgFBSignOut.equals(v))
		 {
			 
			 if(!Utility.isNetworkReacheable(this))
			 {
				 Utility.showToast(this, Preferences.TOAST_NOCONNECT);
				 return;
			 }
			 alertView(this);
			 /*
			Preferences.access_expires = 0;
			Preferences.access_token = null;
			Preferences.savePreferences(getApplicationContext());

			 Intent intent = new Intent(Setting.this, LogoView.class);
			 intent.putExtra("logout", "request_logout");
			 startActivity(intent);
			 */

		 }
		 if(mBtnMoreTips.equals(v))
		 {
			 stopService(new Intent(this, MediaPlayerService.class));
			 Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/MadMathz"));
			 startActivity(browserIntent);
			 isFromTips = true;
		 }
	}
	
	public boolean isFromTips = false;
	
	public void alertView(Context context)
	{
	       /* Alert Dialog Code Start*/     
	            AlertDialog.Builder alert = new AlertDialog.Builder(context);
	            alert.setTitle("Sign Out From Facebook"); //Set Alert dialog title here
	            alert.setMessage("Are You Sure?"); //Message here
	 
	            alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int whichButton) {
	             // String value = input.getText().toString();
	              // Do something with value!
	                //You will get input data in this variable. 
	    			Preferences.access_expires = 0;
	    			Preferences.access_token = null;
	    			Preferences.savePreferences(getApplicationContext());

	    			 Intent intent = new Intent(Setting.this, LogoView.class);
	    			 intent.putExtra("logout", "request_logout");
	    			 startActivity(intent);

	            	isContinue = true;
	                Setting.this.finish();
	 
	              }
	            });
	 
	            alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
	              public void onClick(DialogInterface dialog, int whichButton) {
	                // Canceled.
	                  dialog.cancel();
	              }
	            });
	            AlertDialog alertDialog = alert.create();
	            alertDialog.show();
	}
	
	public boolean isContinue = false;
	
	Handler mHandler = new Handler();
	
	protected void onResume()
	{
		super.onResume();		
		if(isFromTips)
		{
			if(Preferences.bBGM) startService(new Intent(this, MediaPlayerService.class));
			isFromTips = false;
			
		}

		if(Preferences.isIncomingCall)
		{
			Preferences.isIncomingCall = false;
			if(Preferences.bBGM) startService(new Intent(this, MediaPlayerService.class));
			
			mHandler.post(new Runnable(){ //call process

				@Override
				public void run() {
					// TODO Auto-generated method stub
					
			    	LayoutInflater mInflater = (LayoutInflater) Setting.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			    	View inflatedView = mInflater.inflate(R.layout.phone_call, null);
			    	
			    	final RelativeLayout loadingView = (RelativeLayout)findViewById(R.id.loadingView);
			    	loadingView.setVisibility(View.VISIBLE);
			    	loadingView.setBackgroundColor(0xB2000000);
			    	loadingView.removeAllViews();
			    	
			    	loadingView.addView(inflatedView);
			    	
			    	ImageButton btn_resume = (ImageButton)inflatedView.findViewById(R.id.btn_resume);
			    	btn_resume.setOnTouchListener(new ButtonHighlight(btn_resume));
			    	btn_resume.setOnClickListener(new OnClickListener(){

						@Override
						public void onClick(View arg0) {
							// TODO Auto-generated method stub
							loadingView.setVisibility(View.INVISIBLE);
							loadingView.removeAllViews();
						}});
			    	
			    	ImageButton btn_quit = (ImageButton)inflatedView.findViewById(R.id.btn_quit);
			    	btn_quit.setOnTouchListener(new ButtonHighlight(btn_quit));
			    	
			    	btn_quit.setOnClickListener(new OnClickListener(){

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							stopService(new Intent(Setting.this, MediaPlayerService.class));
							Setting.this.finish();
							
						}});
				}});
		}
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		  super.onActivityResult(requestCode, resultCode, data);
		  
		  if(resultCode == Preferences.STATE_CLOSE_APP)
		  {
			  this.finish();
		  }
	}
	
	protected void onStop()
	{
		super.onStop();
		if(Preferences.isIncomingCall)
		{
			stopService(new Intent(this, MediaPlayerService.class));
			Preferences.savePreferences(getApplicationContext());
		}else if(!isContinue) //other reason to shut down 
		{
			stopService(new Intent(this, MediaPlayerService.class));
			this.finish();
		}
		
		
	}
	
	/*

	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if(keyCode == KeyEvent.KEYCODE_BACK)
		{
			stopService(new Intent(this, MediaPlayerService.class));
		}
		
		Preferences.savePreferences(getApplicationContext());
		return super.onKeyDown(keyCode, event);	
	}
	*/

}

