package com.facebook.madmathz;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;

import com.facebook.*;
import com.facebook.madmathz.connector.*;

public class Preferences {
	
	public static final String SENDER_ID = "358811948301";
	
	//HERE DESCRIBES THE SETTINGS FOR THE GAME
	public static final String PREFS_NAME = "MadMathz";
	
	public static String isLoggedIn_str = "isLoggedIn";
	public static boolean isLoggedIn = false;
	public static String Name = "";
	public static String UserName = "";
	public static String FBID = "";
	public static String FirstName = "";
	public static String LastName = "";
	public static String Gender = "";
	public static String Email = "";
	public static String PushToken = "";
	public static String UpdatedTime = "";
	public static boolean Verified = false;
	public static String UserPic = "";
	
	public static boolean isRegistered = false;
	
	public static int Score = 0;
	
	public static int Result = 0;
	public static String Access_Token_Str = "access_token"; 
	public static String Access_Expires_Str = "access_expires";
	public static String access_token = "";
	public static long access_expires = 0;
	
	public static boolean isIncomingCall = false;
	
	public static boolean bBGM = true;
	public static boolean bSound = true;
	public static String bBGM_Str = "BGM";
	public static String bSound_Str = "sound";
	
	public static FacebookConnector mFBConnector = null;
	public static Context mContext = null;
	public static Activity mActivity = null;
	
	public static String FACEBOOK_PERMISSION = "public_stream";
	
	
	
	// WEB Service Definition
	public static final String webServerUrl = "http://121.200.241.83/mm/request_user_details.php?";
	public static final String selectUserScoreByFBID = webServerUrl + "mName=selectScoreByFBID&format=json";
	public static final String updateUserPushToken = webServerUrl + "mName=updateUserPushToken&format=json";
	public static final String selectAllUserFBIDAndScore = webServerUrl + "mName=selectAllUserFBIDAndScore&format=json";
	public static final String selectGamePlayByID = webServerUrl + "mName=selectGamePlayByID&format=json";
	public static final String selectSingleGameRandomly = webServerUrl + "mName=selectSingleGameRandomly&format=json";
	public static final String insertSingleMatchDetails = webServerUrl + "mName=insertSMatchDetails&format=json";
	public static final String updateUserScore = webServerUrl + "mName=updateUserScore&format=json";
	public static final String selectChallengeResultByFBID = webServerUrl + "mName=selectChallengeResultByFBID&format=json";
	public static final String selectChallengeByFBID = webServerUrl + "mName=selectChallengeByFBID&format=json";
	public static final String insertFPlayerDetails = webServerUrl + "mName=insertFPlayerDetails&format=json";
	public static final String insertSPlayerDetails = webServerUrl + "mName=insertSPlayerDetails&format=json";
	public static final String insertNewUser = webServerUrl + "mName=insertNewUser&format=json";
	public static final String selectSingleGameRandomly_v2 = webServerUrl + "mName=selectSingleGameRandomly_v2&format=json";
	public static final String getTotalMatchResult = webServerUrl + "mName=getTotalMatchResult&format=json";
	
	public static final String FacebookPicUrl = "http://graph.facebook.com/%s/picture";
	
	public static final String str_NOCONNECT = "Please connect to internet!";
	public static final int TOAST_NOCONNECT = 1;
	
	public static final int STATE_CLOSE_APP = 1;
	
	//define JSON Tag 
	public static final String s_FBID = "FBID";
	public static final String s_posts = "posts";
	public static final String s_user = "user";
	public static final String s_score = "score";
	public static final String s_FirstName = "FirstName";
	public static final String s_LastName = "LastName";
	public static final String s_Gender = "Gender";
	public static final String s_Email = "Email";
	public static final String s_FBUsername = "FBUsername";
	public static final String s_PushToken = "PushToken";
	public static final String s_fplayer_id = "fplayer_id";
	public static final String s_splayer_id = "splayer_id";
	public static final String s_fplayer_msg = "fplayer_msg";
	public static final String s_splayer_comp_time = "splayer_comp_time";
	public static final String s_fpalyer_comp_poss = "fplayer_comp_poss";
	public static final String s_splayer_comp_poss = "splayer_comp_poss";
	public static final String s_match_played_time = "match_played_time";
	public static final String s_fplayer_wrong_tried = "fplayer_wrong_tried";
	public static final String s_splayer_wrong_tried = "splayer_wrong_tried";
	public static final String s_gameplay_id = "gameplay_id";
	public static final String s_match_id = "match_id";
	public static final String s_status = "status";
	public static final String s_fplayer_username = "fplayer_username";
	public static final String s_splayer_username = "splayer_username";
	public static final String s_fplayer_repeat = "fplayer_repeat";
	public static final String s_splayer_repeat = "splayer_repeat";
	public static final String s_result = "result";
	public static final String s_DiffTime = "DiffTime";
	public static final String s_game_play = "game_play";
	public static final String s_id = "id";
	public static final String s_img_number1 = "img_number1";
	public static final String s_img_number2 = "img_number2";
	public static final String s_img_number3 = "img_number3";
	public static final String s_img_number4 = "img_number4";
	public static final String s_img_number5 = "img_number5";
	public static final String s_img_number6 = "img_number6";
	public static final String s_img_number7 = "img_number7";
	public static final String s_img_number8 = "img_number8";
	public static final String s_img_number9 = "img_number9";
	public static final String s_img_number10 = "img_number10";
	public static final String s_img_number11 = "img_number11";
	public static final String s_img_number12 = "img_number12";
	public static final String s_img_number13 = "img_number13";
	public static final String s_img_number14 = "img_number14";
	public static final String s_img_number15 = "img_number15";
	public static final String s_img_number16 = "img_number16";
	public static final String s_answer = "answer";
	public static final String s_total_possibility = "total_possibility";
	public static final String s_possibility_3 = "possibility_3";
	public static final String s_possibility_4 = "possibility_4";
	public static final String s_GPID = "GPID";
	public static final String s_FID = "FID";
	public static final String s_SID = "SID";
	public static final String s_totalMatchResult = "totalMatchResult";
	public static final String s_winner = "winner";
	
	public static final String FacebookUserProfileUrl = "https://graph.facebook.com/me?access_token=%s&format=json&fields=name,+first_name,+last_name,+email,+updated_time,+gender";
	public static final String FacebookFriendProfileUrl = "https://graph.facebook.com/me/friends?access_token=%s&format=json&fields=name,+picture,+location";
	
	public static final int tx_color = 0xFF5A4200;
	public static final int tx_color_red = 0xFFE5510F;
	
	public static Bitmap mUserPic = null;
	
	public static ChallengeList cur_ChallengeList;
	

	public static void setActivity(Activity _activity)
	{
		mActivity = _activity;
	}
	
	public static void setContext(Context _context)
	{
		mContext = _context;		
	}
	
	public static Typeface tf_bold = null;
	public static Typeface tf_normal = null;
	public static Typeface tf_adonais = null;
	
	public static HashMap<String, Bitmap> FriendsPic = new HashMap<String, Bitmap>();

	
	//load data from sharedPreferences to Global
	public static void loadPreferences(Context context)
	{
		SharedPreferences prefs = context.getSharedPreferences(Preferences.PREFS_NAME, Context.MODE_WORLD_READABLE);
		
		tf_bold = Typeface.createFromAsset(context.getAssets(), "font/helveticaneueltstd-bd-webfont.ttf");
		tf_normal =  Typeface.createFromAsset(context.getAssets(), "font/HelveticaNeue-Roman.ttf");  
		tf_adonais = Typeface.createFromAsset(context.getAssets(), "font/Adonais.ttf");
		
		Preferences.bBGM = prefs.getBoolean("bBGM", true);
		Preferences.bSound = prefs.getBoolean("bSound", true);
		// test
		Preferences.isRegistered = prefs.getBoolean("isRegistered", false);
		Preferences.isRegistered = true;
		Preferences.UserName = prefs.getString("UserName", "");

		Preferences.FBID = prefs.getString("FBID", "");
		
        Preferences.LastName = prefs.getString("last_name", "");
        Preferences.FirstName = prefs.getString("first_name", "");
        Preferences.Gender = prefs.getString("gender", "");
        Preferences.UpdatedTime = prefs.getString("updated_time", ""); 
        Preferences.PushToken = prefs.getString("PushToken", "");
        Preferences.access_token = prefs.getString("access_token", "");
        Preferences.Score = prefs.getInt("Score", 0);
        Preferences.access_expires = prefs.getLong("access_expires", 0);
//        Preferences.Verified = profile.getBoolean("verified");
//        Preferences.UserPic = "http://graph.facebook.com/"+ Preferences.FBID +"/picture";

		
	}
	
	public static void clearPreferences(Context context)
	{
		SharedPreferences prefs = context.getSharedPreferences(Preferences.PREFS_NAME, Context.MODE_WORLD_READABLE);
		Preferences.isRegistered = false;
		
		
	}
	
	//save Global settings to sharedPreferences	
	public static void savePreferences(Context context)
	{
		SharedPreferences.Editor editor = context.getSharedPreferences(Preferences.PREFS_NAME, Context.MODE_WORLD_READABLE).edit();
		editor.putBoolean("isRegistered", Preferences.isRegistered);
		editor.putBoolean("bBGM", Preferences.bBGM);
		editor.putBoolean("bSound", Preferences.bSound);
		editor.putString("FBID", Preferences.FBID);
		editor.putString("UserName", Preferences.UserName);

		editor.putString("first_name", Preferences.FirstName);
		editor.putString("last_name", Preferences.LastName);
		editor.putString("gender", Preferences.Gender);
		editor.putString("updated_time", Preferences.UpdatedTime);
		editor.putString("PushToken", Preferences.PushToken);
		editor.putString("access_token", Preferences.access_token);
		editor.putLong("access_expires", Preferences.access_expires);
		editor.putInt("Score", Preferences.Score);

		editor.commit();
	}
	
	
	
	public Preferences()
	{
	}
	
	
	//User Info 
	public static ArrayList<HashMap<String, String>>  mFriends_List = new ArrayList<HashMap<String, String>>();
	public static ArrayList<HashMap<String, String>> mAllUsers = new ArrayList<HashMap<String, String>>();
    public static ArrayList<HashMap<String, String>> mChallengeList = new ArrayList<HashMap<String, String>>();
    
    
    //Message Text
//    public static String msg_completed = "You completed in %d seconds";
    public static String msg_youwintime = "You Win! %ds faster.";
    public static String msg_youwincombo = "You Win! %d combo more.";
    public static String msg_youlosetime = "You Lose! %ds slow.";
    public static String msg_youlosecombo = "You Lose! %d combo less.";
    public static String msg_youcomplete = "You completed in %d seconds";
    public static String msg_complete = "Completed in %d seconds";
    public static String msg_youhitscore = "You had hit %d score(s) in %d seconds";
    public static String msg_hitscore = "Hit %d score(s) in %d seconds";
    
    public static final int GAME_TIME = 60;
    
    public static HashMap<String, String> AllUserInfo = new HashMap<String, String>(); //fb_id, score

	

}
