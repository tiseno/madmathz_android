package com.facebook.madmathz;

//import static com.example.gcm.CommonUtilities.SENDER_ID;
import java.net.URI;
import java.util.HashMap;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gcm.GCMBaseIntentService;

public class GCMIntentService extends GCMBaseIntentService{

	String c2dm_msg = null;
	
	public GCMIntentService() {
		super(Preferences.SENDER_ID);
	}

	private static final String TAG = "===GCMIntentService===";

	public void onReceive(Context context, Intent intent) {
	   
		
		
	}


	@Override
	protected void onRegistered(Context arg0, String registrationId) {
		Log.i(TAG, "Device registered: regId = " + registrationId);
		
		Preferences.PushToken = registrationId;
		
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("FBID", Preferences.FBID);
		params.put("PushToken", Preferences.PushToken);
		params.put("device_type", "android");
		
		String response = HttpManager.postHttpResponse(URI.create(Preferences.updateUserPushToken), params);
		
		
		/*
		GCM3rdPartyRequest registration_red_id = new GCM3rdPartyRequest();
		registration_red_id.Setting("http://192.168.11.46/gcm/GCMRegistration_Id.php", "3", "lee", registrationId, null);
		registration_red_id.start();
		
		try {
			registration_red_id.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
	}

	@Override
	protected void onUnregistered(Context arg0, String arg1) {
	Log.i(TAG, "unregistered = "+arg1);
	}

	@Override
	protected void onMessage(Context arg0, Intent arg1) {
		Log.i(TAG, "CONTEXT : = " + arg0);
		Log.i(TAG, "new message= ");
	    if (arg1.getAction().equals("com.google.android.c2dm.intent.RECEIVE")) {
	    
	    
	    c2dm_msg = arg1.getExtras().getString("msg");
	    GET_GCM();
	    triggerNotification(arg0, c2dm_msg);
	    
	    
	    //Log.i(TAG,c2dm_msg);
	    
	   }
	
	}
	
	

	public void GET_GCM() { 
	    
	    Thread thread = new Thread(new Runnable() { 
	        public void run() { 
	        	
	        handler.sendEmptyMessage(0); 
	        } 
	    }); 
	    thread.start(); 
	} 
	
	public static int notificationCounter = 0;

	private Handler handler = new Handler() { 
	    public void handleMessage(Message msg) { 
	        Context context = getApplicationContext();
	        int duration = Toast.LENGTH_LONG;
	        Toast toast = Toast.makeText(context, c2dm_msg, duration);
	        toast.show(); 
	        
	        
	        c2dm_msg = null;
	
	    } 
    };
    
    NotificationManager notificationManager;
    
    public void triggerNotification(Context context, String msg) {
    	
        notificationCounter++;
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Intent contentIntent = new Intent(this, com.facebook.madmathz.Splash.class);
        Notification notification = new Notification(R.drawable.icon, msg, System.currentTimeMillis());
        notification.defaults |= Notification.DEFAULT_SOUND; 
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        
        contentIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notification.setLatestEventInfo(this, "MadMathz", c2dm_msg, PendingIntent.getActivity(this.getBaseContext(), 0, contentIntent, PendingIntent.FLAG_CANCEL_CURRENT));
        notificationManager.notify(notificationCounter, notification);
        
/*
	NotificationManager notificationManager = (NotificationManager)context.getSystemService(Activity.NOTIFICATION_SERVICE);
	
	// 해당 어플을 실행하는 이벤트를 하고싶을 때 아래 주석을 풀어주세요
	 PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, 
	 new Intent(context, com.facebook.madmathz.Splash.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK), 0);
	//PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, new Intent(), 0);
	
	Notification notification = new Notification();
	notification.icon = R.drawable.icon;
	notification.when = System.currentTimeMillis();
	notification.vibrate = new long[] { 500, 100, 500, 100 };
	notification.sound = Uri.parse("/system/media/audio/notifications/20_Cloud.ogg");
	notification.flags = Notification.FLAG_AUTO_CANCEL;
	notification.setLatestEventInfo(context, "MadMathz", c2dm_msg, pendingIntent);
	
	notificationManager.notify(0, notification);
*/
}

	public void showMsg(String msg, int option) 
	   { 
	Toast.makeText(this, msg, option).show(); 
	}
	@Override
	protected void onError(Context arg0, String errorId) {
		Log.i(TAG, "Received error: " + errorId);
	}

	@Override
	protected boolean onRecoverableError(Context context, String errorId) {
	return super.onRecoverableError(context, errorId);
	}
}


