package com.facebook.madmathz;
/*
 * ChallengeListAdapter.java
 * 
 * 
 */

import java.util.ArrayList;

import com.facebook.madmathz.FriendsList.ViewHolder;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * ListView Friends ArrayAdapter
 */
public class ChallangeListAdapter extends ArrayAdapter<ChallengeList> implements OnClickListener {
    private final Activity context;
    private final ArrayList<ChallengeList> challengelists;
    private int resourceId;

    /**
     * Constructor
     * @param context the application content
     * @param resourceId the ID of the resource/view
     * @param friends the bound ArrayList
     */
    public ChallangeListAdapter(
            Activity context, 
            int resourceId, 
            ArrayList<ChallengeList> challengelists) {
        super(context, resourceId, challengelists);
        this.context = context;
        this.challengelists = challengelists;
        this.resourceId = resourceId;
        mInflater = LayoutInflater.from(context.getBaseContext());
    }
	
	/*private view holder class*/
    private class ViewHolder {
        ImageView UserThumbnail;
        TextView UserName;
        TextView ChallengeTime;
        TextView postTime;
		ImageButton challengeBtn;
    }
    /**
     * Updates the view
     * @param position the ArrayList position to update
     * @param convertView the view to update/inflate if needed
     * @param parent the groups parent view
     */
    /* (non-Javadoc)
     * @see android.widget.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
     */
    private LayoutInflater mInflater;
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	 View rowView = convertView;
         if (convertView == null) {
        	 rowView = mInflater.inflate(resourceId, null);
             ViewHolder holder = new ViewHolder();
             holder.UserThumbnail = (ImageView) rowView.findViewById(R.id.UserThumbnail);
             holder.UserName = (TextView) rowView.findViewById(R.id.UserName);
             holder.UserName.setTextColor(Preferences.tx_color);
             holder.UserName.setTypeface(Preferences.tf_normal, Typeface.BOLD);
             
             holder.ChallengeTime = (TextView) rowView.findViewById(R.id.ChallengeTime);
             holder.ChallengeTime.setTextColor(Preferences.tx_color);
             holder.ChallengeTime.setTypeface(Preferences.tf_normal);
             
     		holder.postTime = (TextView)rowView.findViewById(R.id.PostTime);
     		holder.postTime.setTypeface(Preferences.tf_normal);
     		holder.postTime.setTextColor(Preferences.tx_color);

             
             holder.challengeBtn = (ImageButton)rowView.findViewById(R.id.ChallengeBtn);
             holder.challengeBtn.setOnTouchListener(new ButtonHighlight(holder.challengeBtn));
         
             rowView.setTag(holder);
         }

         ViewHolder holder = (ViewHolder) rowView.getTag();
    	
         /*
        View rowView = convertView;
        
        if (rowView == null) {
            LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = vi.inflate(resourceId, null);
        }
        */
        
        
        ChallengeList f = challengelists.get(position);
        /*
		ImageView imageView = (ImageView)rowView.findViewById(R.id.UserThumbnail);
		TextView userName = (TextView)rowView.findViewById(R.id.UserName);
		userName.setTextColor(Preferences.tx_color);
		userName.setTypeface(Preferences.tf_normal, Typeface.BOLD);
		
		TextView time = (TextView) rowView.findViewById(R.id.ChallengeTime);
		time.setTextColor(Preferences.tx_color_red);
		time.setTypeface(Preferences.tf_normal);
		
		TextView postTime = (TextView)rowView.findViewById(R.id.PostTime);
		postTime.setTypeface(Preferences.tf_normal);
		postTime.setTextColor(Preferences.tx_color);
		
		Button challengeBtn = (Button)rowView.findViewById(R.id.ChallengeBtn);
		*/
		
		boolean isCompleted = f.fplayer_comp_time >= Preferences.GAME_TIME;
		
		//adjust items
		boolean isFirstUser = false;
		String posttime_str;
		if(f.DiffTime / 3600 > 0)
		{
			posttime_str = "" + f.DiffTime / 3600 + "hours ago";
		}
		else
		{
			posttime_str = "" + f.DiffTime % 3600 / 60 + "miniutes ago" ;
		}
		
		if(f.fplayer_id.equals(Preferences.FBID))
		{
			//User is in waiting mode
			isFirstUser = true;
			
			//imageView.setImageResource(resId); //userpicture
			holder.UserThumbnail.setTag(String.format(Preferences.FacebookPicUrl, f.fplayer_id));
			new GetPicAsyncTask().execute(holder.UserThumbnail);
			holder.UserName.setText(f.fplayer_username);
			holder.ChallengeTime.setText(String.format(Preferences.msg_youhitscore, f.fplayer_comp_poss, f.fplayer_comp_time));
			holder.challengeBtn.setBackgroundResource(R.drawable.btn_waiting);
			
		}
		else
		{
			isFirstUser = false;
			//imageView.setImageResource(resId); //friend's picture
			holder.UserThumbnail.setTag(String.format(Preferences.FacebookPicUrl, f.fplayer_id));
			new GetPicAsyncTask().execute(holder.UserThumbnail);
			holder.UserName.setText(f.fplayer_username);
			holder.ChallengeTime.setText(String.format(Preferences.msg_hitscore, f.fplayer_comp_poss, f.fplayer_comp_time));
			
			holder.challengeBtn.setBackgroundResource(R.drawable.btn_accept);
		}
		holder.postTime.setText(posttime_str);
		//imageView.setImageResource(Preferences.UserPic);
		
        return rowView;
    }
    
		
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		
	}   



}
