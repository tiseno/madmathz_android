/**
 * Copyright 2012 Facebook
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facebook.madmathz;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;
import android.os.AsyncTask;
import android.widget.BaseAdapter;

import java.util.Hashtable;
import java.util.Stack;

/*
 * Fetch friends profile pictures request via AsyncTask
 */
public class FriendsGetProfilePics {

    Hashtable<String, Bitmap> friendsImages;
    Hashtable<String, String> positionRequested;
    BaseAdapter listener;
    int runningCount = 0;
    Stack<ItemPair> queue;

    /*
     * 15 max async tasks at any given time.
     */
    final static int MAX_ALLOWED_TASKS = 15;

    public FriendsGetProfilePics() {
        friendsImages = new Hashtable<String, Bitmap>();
        positionRequested = new Hashtable<String, String>();
        queue = new Stack<ItemPair>();
    }

    /*
     * Inform the listener when the image has been downloaded. listener is
     * FriendsList here.
     */
    public void setListener(BaseAdapter listener) {
        this.listener = listener;
        reset();
    }

    public void reset() {
        positionRequested.clear();
        runningCount = 0;
        queue.clear();
    }

    /*
     * If the profile picture has already been downloaded and cached, return it
     * else execute a new async task to fetch it - if total async tasks >15,
     * queue the request.
     */
    public Bitmap getImage(String uid, String url) {
        Bitmap image = friendsImages.get(uid);
        if (image != null) {
            return image;
        }
        if (!positionRequested.containsKey(uid)) {
            positionRequested.put(uid, "");
            if (runningCount >= MAX_ALLOWED_TASKS) {
                queue.push(new ItemPair(uid, url));
            } else {
                runningCount++;
                new GetProfilePicAsyncTask().execute(uid, url);
            }
        }
        return null;
    }

    public void getNextImage() {
        if (!queue.isEmpty()) {
            ItemPair item = queue.pop();
            new GetProfilePicAsyncTask().execute(item.uid, item.url);
        }
    }

    /*
     * Start a AsyncTask to fetch the request
     */
    private class GetProfilePicAsyncTask extends AsyncTask<Object, Void, Bitmap> {
        String uid;

        @Override
        protected Bitmap doInBackground(Object... params) {
            this.uid = (String) params[0];
            String url = (String) params[1];
            return getRoundedCornerImage(Utility.getBitmap(url));
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            runningCount--;
            if (result != null) {
                friendsImages.put(uid, result);
                listener.notifyDataSetChanged();
                getNextImage();
            }
        }
    }
    

    public static Bitmap getRoundedCornerImage(Bitmap bitmap) {
	   	 Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
	   	     bitmap.getHeight(), Config.ARGB_8888);
	   	 Canvas canvas = new Canvas(output);
	
	   	 final int color = Preferences.tx_color; //0xff424242;
	   	 final Paint paint = new Paint();
	   	 final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
	   	 final RectF rectF = new RectF(rect);
	   	 final float roundPx = 10;
	
	   	 paint.setAntiAlias(true);
	   	 canvas.drawARGB(0, 0, 0, 0);
	   	 paint.setColor(color);
	   	 canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
	
	   	 paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
	   	 canvas.drawBitmap(bitmap, rect, rect, paint);
	   	 
	   	 //border
	   	 paint.setColor(Preferences.tx_color);
	   	 paint.setStyle(Paint.Style.STROKE);
	   	 paint.setStrokeWidth(2);
	   	 canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
	
	   	 return output;
  	 }

    class ItemPair {
        String uid;
        String url;

        public ItemPair(String uid, String url) {
            this.uid = uid;
            this.url = url;
        }
    }

}
